<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>ViewPatientFiles</title>
</head>

<body>
	<style>
.container {
	padding: 25px;
	background-color: lightblue;
	border-style: double;
}

.textcont {
	border-style: double;
	background-color: #e1e2f9;
}

#drnotes {
	height: 200px;
	width: 1100px;
	font-size: 12pt;
	text-align: left;
}

th, td, tr {
	border: 1.5px solid black;
	heigth: 50px;
	width: 1100px;
	padding: 10px;
	text-align: center;
	background-color: #e1e2f9;
}

p {
	background-color: white;
}

td {
	padding: 15px;
}

#dname, #pat, #hcn, #adate {
	padding: 10px;
}

p {
	height: 75px;
	width: 1100px;
	font-size: 12pt;
	text-align: left;
	border: solid blue;
}

Body {
	font-family: Calibri, Helvetica, sans-serif;
	background-color: rgba(255, 255, 255, 0.2);
	background-image:
		url("https://i.pinimg.com/originals/1a/ee/5c/1aee5c344846f449350feae457ea350e.jpg");
	background-blend-mode: lighten;
	background-size: 100%;
	background-size: cover;
	background-position: center center;
	background-repeat: no-repeat;
}
</style>

	<div class="container">
	
		<form>
			<h1 style="text-align: center;"><%=session.getAttribute("prac")%></h1>
			<h2 style="text-align: center;"><%=session.getAttribute("loc")%></h2>

			<div style="border: double;">
				<center>
					<table>
						<tr>
							<th><b>Doctor</b></th>
							<th><b>Patient</b></th>
							<th><b>Patient Health Card Number</b></th>
							<th><b>Date of Appointment</b></th>
						</tr>

						<tr>
							<td><center>Dr. <%=session.getAttribute("dName")%></center></td>
							
							<td><center><%=session.getAttribute("pName")%></center></td>
							
							<td><center><%=session.getAttribute("hcnum")%></center></td>
							
							<td><center><%=session.getAttribute("date")%></center></td>
						</tr>
					</table>
				</center>
			</div>
			<br> <br>
			
			<div class="textcont">
				<center>
					<br> <b>Visit Notes / Summary </b><br>
					<p><%=session.getAttribute("notes")%></p>
					<br> 
					
					<br> <b>Diagnosis / Prescriptions (N/A if none)</b>
					<p><%=session.getAttribute("rxs")%></p>
					<br> <br> 
					
					<b>Tests Requested / Referrals (N/A if none)</b>
					<p><%=session.getAttribute("referrals")%></p>
					<br> <br> 
					
					<b>Follow Up Scheduled (N/A if none)</b>
					<p><%=session.getAttribute("follows")%></p>
					<br> <br>
				</center>
			</div>
		</form>
	</div>
</body>
</html>