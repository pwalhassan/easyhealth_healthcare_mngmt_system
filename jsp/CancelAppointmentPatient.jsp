<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="java.sql.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cancel an Appointment</title>
</head>

<body>
	<style>
.container {
	padding: 25px;
	background-color: lightblue;
}

Body {
	font-family: Calibri, Helvetica, sans-serif;
	background-color: rgba(255, 255, 255, 0.2);
	background-image:
		url("https://i.pinimg.com/originals/1a/ee/5c/1aee5c344846f449350feae457ea350e.jpg");
	background-blend-mode: lighten;
	background-size: 100%;
	background-size: cover;
	background-position: center center;
	background-repeat: no-repeat;
}
</style>

	<center>
		<h1>Cancel an appointment</h1>
	</center>

	<form method="post" action="CancelAppointmentP">
		<div class="container">

			<label>Which appointment would you like to cancel? </label> <select
				name="CancelAppt" id="CancelAppt">
				<option value="-1">Select the Appointment</option>
				<%
				try {
					Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
					Connection Con = DriverManager.getConnection("jdbc:mysql://localhost:3306/signupinfo", "root",
					"29inWonderlandWoohoo21");
					String data = "SELECT * From appointments WHERE pName = '" + session.getAttribute("Name") + "'";
					Statement stmta = Con.createStatement();
					ResultSet appts = stmta.executeQuery(data);

					while (appts.next()) {
				%>
				<option
					value="<%=appts.getString("aDate") + "/" + appts.getString("aTime") + "/" + appts.getString("dFName")%>"><%=appts.getString("aDate") + " " + appts.getString("aTime") + " " + appts.getString("dFName")%></option>
				<%
				}
				} catch (Exception e) {
				e.printStackTrace();
				out.println("Error" + e.getMessage());
				}
				%>
			</select><br> <br>

			<button type="submit">Cancel Appointment</button>

		</div>
	</form>
</body>
</html>