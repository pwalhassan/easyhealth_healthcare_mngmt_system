<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>DoctorSignUp</title>
</head>

<body>
	<style>
Body {
	font-family: Calibri, Helvetica, sans-serif;
	background-color: rgba(255, 255, 255, 0.2);
	background-image:
		url("https://i.pinimg.com/originals/1a/ee/5c/1aee5c344846f449350feae457ea350e.jpg");
	background-blend-mode: lighten;
	background-size: 100%;
	background-size: cover;
	background-position: center center;
	background-repeat: no-repeat;
}

button {
	background-color: #3BCF50;
	width: 100%;
	padding: 7px;
	margin: 15px 0px;
	cursor: pointer;
}

.resetbttn {
	width: auto;
	padding: 10px 18px;
	margin: 10px 5px;
	background-color: white;
}

form {
	border: 3px solid #f1f1f1;
}

input[type=text], input[type=password], input[type=email], #dspec {
	width: 100%;
	margin: 8px 0;
	padding: 12px 20px;
	display: inline-block;
	border: 2px solid blue;
	box-sizing: border-box;
}

.container {
	padding: 25px;
	background-color: lightblue;
}
</style>

	<center>
		<h2>Fill out the form below to create your account!</h2>
	</center>
	<br>
	<br>

	<form action="createDoctor" method="post">
		<div class="container">
		
			<label for="dusername">Username :</label><br> 
			<input type="text" name="dusername" required><br> <br> 
			
			<label for="demail">Email :</label><br> 
			<input type="email" name="demail" required><br> <br> 
			
			<label for="dpword">Password :</label><br> 
			<input type="password" name="dpword" id="pword" required><br> <br> 
			
			<label for="drepword">Re-enter Password :</label><br> 
			<input type="password" name="drepword" id="repword" required><br>
			<br> <br> <br> 
			
			<label for="fname">First Name :</label><br> 
			<input type="text" placeholder="First Name" name="fname" required><br> <br> 
			
			<label for="mname">Middle name :</label><br> 
			<input type="text" placeholder="Middle Name" name="mname"><br> <br> 
			
			<label for="lname">Last Name :</label><br> 
			<input type="text" placeholder="Last Name" name="lname" required><br> <br> 
			
			<label for="dsex">Sex :</label><br> 
			<input type="radio" id="male" name="dsex" value="Male" required> 
			<label for="male">Male</label><br> 
			<input type="radio" id="female" name="dsex" value="Female"> 
			<label for="female">Female</label><br> <br>
			 
			<label for="dspec">Specialty :</label><br> 
			<select name="dspec" id="dspec">
				<option value="-1">Please Select your Specialty</option>
				<option value="Allergy">Allergy</option>
				<option value="Cardiology">Cardiology</option>
				<option value="Dermatology">Dermatology</option>
				<option value="Emergency">Emergency</option>
				<option value="Endocrinology">Endocrinology</option>
				<option value="ENT">ENT</option>
				<option value="General">General</option>
				<option value="Gynecology">Gynecology</option>
				<option value="Hemotology">Hemotology</option>
				<option value="Infectious Disease">Infectious Disease</option>
				<option value="Internal Medicine">Internal Medicine</option>
				<option value="Neurology">Neurology</option>
				<option value="Obstetrics">Obstetrics</option>
				<option value="Oncology">Oncology</option>
				<option value="Orthopedics">Orthopedics</option>
				<option value="Pathology">Pathology</option>
				<option value="Pediatric">Pediatric</option>
				<option value="Psychiatry">Psychiatry</option>
				<option value="Radiology">Radiology</option>
				<option value="Rheumatology">Rheumatology</option>
				<option value="Surgery">Surgery</option>
			</select><br> <br> 
			
			<label for="dlicnum">License Number (MINC) :</label><br> 
			<input type="text" placeholder="ABCD-EFGH-9999" name="dlicnum" required><br><br> 
			
			<label for="dprac">Practice Name :</label><br> 
			<input type="text" placeholder="Your Practice Name" name="dprac" required><br><br>
			
			<label for="dloc">Practice Location :</label><br> 
			<input type="text" placeholder="Your Practice Location" name="dloc" required><br> <br>

			<button type="submit">Complete My Profile</button>
			<button type="reset" class="resetbttn">Reset</button>
		</div>
	</form>
</body>
</html>