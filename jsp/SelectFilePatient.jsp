<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Select Files</title>
</head>

<body>
	<style>
.container {
	padding: 25px;
	background-color: lightblue;
}

table, th, td, tr {
	border: 1px solid white;
}

th, td, tr {
	background-color: #e1f9e9;
}

button {
	background-color: #e1e2f9;
}

Body {
	font-family: Calibri, Helvetica, sans-serif;
	background-color: rgba(255, 255, 255, 0.2);
	background-image:
		url("https://i.pinimg.com/originals/1a/ee/5c/1aee5c344846f449350feae457ea350e.jpg");
	background-blend-mode: lighten;
	background-size: 100%;
	background-size: cover;
	background-position: center center;
	background-repeat: no-repeat;
}
</style>

	<form method="post" action="ViewFileServletPatient">
		<h1>
			<center>Select Which File To View</center>
		</h1>
		
		<div class="container">
		
			<center>
				<table>
					<%
					int num = (Integer) session.getAttribute("numFiles");
					int filenum = 0;
					String sizesel = Integer.toString(num);
					Object names = session.getAttribute("pnames");
					Object dates = session.getAttribute("dates");
					List<String> pnames = (ArrayList<String>) names;
					List<String> adates = (ArrayList<String>) dates;
					%>
					<h2>Patient: <%=pnames.get(0)%>'s Files
					</h2>
					<input type="hidden" name="pname" value="<%=pnames.get(0)%>">

					<div>
						<tr>
							<th>File Number</th>
							<th>Date of Appointment</th>
							<th>Link to File</th>
						</tr>
						
						<%
						for (int i = 0; i < num; i++) {
							filenum = filenum + 1;
							%>
							<tr>
								<td><label for="filenum">
								<center><%="File " + Integer.toString(filenum)%></center>
								</label></td>
								
								<td><center><%=adates.get(i)%></center></td>
								
								<td><center>
									<button value="<%=Integer.toString(filenum)%>" name="filenum">View This File
									</button>
								</center></td>
							</tr>
							<%
						}
						%>
					</select>
				</table>
			</center>
		</div>
	</form>
</body>
</html>