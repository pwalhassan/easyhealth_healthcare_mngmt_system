<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Patient Dashboard</title>
</head>

<body>
	<style>
Body {
	font-family: Calibri, Helvetica, sans-serif;
	background-color: rgba(255, 255, 255, 0.2);
	background-image:
		url("https://i.pinimg.com/originals/1a/ee/5c/1aee5c344846f449350feae457ea350e.jpg");
	background-blend-mode: lighten;
	background-size: 100%;
	background-size: cover;
	background-position: center center;
	background-repeat: no-repeat;
}

form {
	border: 3px solid #f1f1f1;
}

.container {
	padding: 25px;
	background-color: lightblue;
}

.UpcomingEventsContainer {
	padding: 25px;
	border: 3px solid black;
	background-color: #e1e2f9;
}

.threebuttons {
	display: inline-block;
	margin: 3px;
}

input[type=LogOutButton] {
	background-color: #e1f9e9;
	border: 1px solid black;
	width: 60px;
	padding: 7px;
	margin: 15px 0px;
	cursor: pointer;
	position: absolute;
	top: 10px;
	right: 10px;
	text-align: center;
}

input[type=button] {
	background-color: #e1f9e9;
	border: 1px solid black;
	width: 391px;
	padding: 7px;
	margin: 15px 0px;
	cursor: pointer;;
}

table, th, td, tr {
	border: 1px solid black;
}

th, td, tr {
	background-color: lightblue;
}

input[type=button], [type=submit] {
	background-color: #e1f9e9;
	border: 1px solid black;
	width: 359px;
	padding: 7px;
	margin: 15px 0px;
	cursor: pointer;;
}

.SubmitButton {
	background-color: #e1f9e9;
	width: 250px;
	padding: 7px;
	margin: 15px 0px;
	cursor: pointer;
}
</style>

	<a href="http://localhost:8080/EasyHealth/PatientOrDoctor.jsp"> <input
		type="LogOutButton" value="Log Out" />
	</a>

	<center>
		<h1>Patient Dashboard</h1>
	</center>

	<form method="post" action="GetPatientFilesServlet">
		<div class="container">
		
			<center>
				<input type="hidden" name="pnamefile" value="<%=session.getAttribute("Name")%>"> 
					
				<a class="threebuttons"
					href="http://localhost:8080/EasyHealth/PatientProfile.jsp"> 
					<input type="button" value="View your profile" />
				</a>
				
				<a class="threebuttons"
					href="http://localhost:8080/EasyHealth/BookAppointment.jsp"> 
					<input type="button" value="Book an Appointment" />
				</a> 
				
				<a class="threebuttons"> <input type="submit"
					value="View your Files" />
				</a>
			</center>
			
			<div class="UpcomingEventsContainer">
			
				<center>
					<h1 style="color: black; font-size: 30px;">Upcoming Appointments</h1>
				</center>
				
				<center>
					<table>
						<%
						try {
							Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
							Connection Con = DriverManager.getConnection("jdbc:mysql://localhost:3306/signupinfo", "root",
							"29inWonderlandWoohoo21");
							String data = "SELECT * FROM appointments WHERE pName='" + session.getAttribute("Name") + "'ORDER BY aDate ASC";
							Statement stmtd = Con.createStatement();
							ResultSet upApps = stmtd.executeQuery(data);
							
							if (upApps.isBeforeFirst()) {
							%>
							<tr>
								<th>Appointment Date</th>
								<th>Appointment Time</th>
								<th>Doctor Name</th>
							</tr>
							
								<%
								while (upApps.next()) {
									%>
									<tr>
										<center>
											<td><%=upApps.getString("aDate")%></td>
										</center>
										
										<center>
											<td><%=upApps.getString("aTime")%></td>
										</center>
										
										<center>
											<td><%=upApps.getString("dFName")%></td>
										</center>
									</tr>
									<%
								}
							}
						} catch (Exception e) {
						e.printStackTrace();
						out.println("Error" + e.getMessage());
						}
						%>
					</table>
				</center>
				
				<center>
					<a href="http://localhost:8080/EasyHealth/CancelAppointmentPatient.jsp">
						<input type="button" value="Cancel an Appointment" />
					</a>
				</center>
				
			</div>
		</div>
	</form>
	<br>
	<br>
	
	<div class="UpcomingEventsContainer">
		<form method="post" action="BookReferralServlet">
		
			<center>
				<h1 style="color: black; font-size: 30px;">Referrals</h1>
			</center>

			<%
			try {
				Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
				Connection Con = DriverManager.getConnection("jdbc:mysql://localhost:3306/signupinfo", "root",
				"29inWonderlandWoohoo21");
				String data = "SELECT * From referrals WHERE pName= '" + session.getAttribute("Name") + "'";
				Statement stmtr = Con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				ResultSet ref = stmtr.executeQuery(data);

				if (ref.next()) {
					%><label for="dName">Doctors you have referrals for:</label>
					<%
					ref.beforeFirst();
				} 
				
				else {
					%>You have no referrals right now!<%
				}
	
				while (ref.next()) {
					%>
					<input type="radio" id="<%=ref.getString("dName")%>" name="dName"
						value="<%=ref.getString("dName")%>"> <label for="dName"><%=ref.getString("dName")%></label>
					<%
				}
	
				ref.beforeFirst();
				if (ref.next()) {
					%><br> <br> <label for="refDate"> Pick a date for
						the appointment: </label> <input type="date" id="refDate" name="refDate">
					<br> <br>
					<center>
						<button type="submit">Book Referral Appointment</button>
					</center>
					<%
				}
			} 
			
			catch (Exception e) {
				System.out.println(e.getMessage());
			}
			%>
		</form>
	</div>
</body>
</html>