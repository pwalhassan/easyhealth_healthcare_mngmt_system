<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>

<body>
	<style>
Body {
	font-family: Calibri, Helvetica, sans-serif;
	background-color: rgba(255, 255, 255, 0.2);
	background-image:
		url("https://i.pinimg.com/originals/1a/ee/5c/1aee5c344846f449350feae457ea350e.jpg");
	background-blend-mode: lighten;
	background-size: 100%;
	background-size: cover;
	background-position: center center;
	background-repeat: no-repeat;
}

form {
	border: 3px solid #f1f1f1;
}

.container {
	padding: 25px;
	background-color: lightblue;
}

.UpcomingEventsContainer {
	padding: 25px;
	border: 3px solid black;
	background-color: #e1e2f9;
}

.threebuttons {
	display: inline-block;
	margin: 3px;
}

input[type=LogOutButton] {
	background-color: #e1f9e9;
	border: 1px solid black;
	width: 60px;
	padding: 7px;
	margin: 15px 0px;
	cursor: pointer;
	position: absolute;
	top: 10px;
	right: 10px;
	text-align: center;
}

input[type=button] {
	background-color: #e1f9e9;
	width: 250px;
	padding: 7px;
	margin: 15px 0px;
	cursor: pointer;;
}

table, th, td, tr {
	border: 1px solid black;
}

th, td, tr {
	background-color: lightblue;
}
</style>

	<a href="http://localhost:8080/EasyHealth/PatientOrDoctor.jsp"> <input
		type="LogOutButton" value="Log Out" />
	</a>

	<center>
		<h1>Doctor Dashboard</h1>
	</center>

	<form method="post">
		<div class="container">

			<center>
				<a class="threebuttons"
					href="http://localhost:8080/EasyHealth/PatientList.jsp"> <input
					type="button" value="View your Patient list" />
				</a><a class="threebuttons"
					href="http://localhost:8080/EasyHealth/DoctorProfile.jsp"> <input
					type="button" value="View your Profile" />
				</a> <a class="threebuttons"
					href="http://localhost:8080/EasyHealth/SendReferral.jsp"> <input
					type="button" value="Send a Referral" />
				</a> <a class="threebuttons"
					href="http://localhost:8080/EasyHealth/SelectPatient.jsp"> <input
					type="button" value="View your Patient's Files" />
				</a>
			</center>

			<div class="UpcomingEventsContainer">
				<center>
					<h1 style="color: black; font-size: 30px; font-style: Algerian;">Upcoming
						Appointments</h1>
				</center>

				<center>
					<table>
						<%
						try {
							Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
							Connection Con = DriverManager.getConnection("jdbc:mysql://localhost:3306/signupinfo", "root",
							"29inWonderlandWoohoo21");
							String data = "SELECT * From appointments WHERE dName='" + session.getAttribute("Name") + "' ORDER BY aDate ASC";
							Statement stmtd = Con.createStatement();
							ResultSet upApps = stmtd.executeQuery(data);
							if (upApps.isBeforeFirst()) {
						%>
						<tr>
							<th>Appointment Date</th>
							<th>Appointment Time</th>
							<th>Patient Name</th>
						</tr>
						<%
						while (upApps.next()) {
						%>
						<tr>
							<center>
								<td><%=upApps.getString("aDate")%></td>
							</center>
							<center>
								<td><%=upApps.getString("aTime")%></td>
							</center>
							<center>
								<td><%=upApps.getString("pName")%></td>
							</center>
						</tr>
						<%
						}
						}
						} catch (Exception e) {
						e.printStackTrace();
						out.println("Error" + e.getMessage());
						}
						%>
					</table>
				</center>

				<center>
					<a
						href="http://localhost:8080/EasyHealth/CancelAppointmentDoctor.jsp">
						<input type="button" value="Cancel an Appointment" />
					</a>
				</center>

			</div>
		</div>
	</form>
</body>
</html>