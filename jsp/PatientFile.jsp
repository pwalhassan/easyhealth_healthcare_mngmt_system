<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>CreateNewPatientFile</title>
</head>

<body>
	<style>
.container {
	padding: 25px;
	background-color: lightblue;
	border-style: double;
}

.textcont {
	border-style: double;
	background-color: #e1e2f9;
}

#drnotes {
	height: 200px;
	width: 1100px;
	font-size: 12pt;
	text-align: left;
}

th, td, tr {
	border: 1.5px solid black;
	heigth: 50px;
	width: 1100px;
	padding: 10px;
	text-align: center;
	background-color: #e1e2f9;
}

td {
	padding: 15px;
}

#dname, #pat, #hcn, #adate {
	padding: 10px;
}

textarea[type=text] {
	height: 75px;
	width: 1100px;
	font-size: 12pt;
	text-align: left;
	border: solid blue;
}

Body {
	font-family: Calibri, Helvetica, sans-serif;
	background-color: rgba(255, 255, 255, 0.2);
	background-image:
		url("https://i.pinimg.com/originals/1a/ee/5c/1aee5c344846f449350feae457ea350e.jpg");
	background-blend-mode: lighten;
	background-size: 100%;
	background-size: cover;
	background-position: center center;
	background-repeat: no-repeat;
}
</style>

	<div class="container">
		<form action="PatientFileServlet" method="post">
		
			<%
			List<String> patHCN = new ArrayList<String>();
			%>

			<h1 style="text-align: center;"><%=session.getAttribute("dPrac")%></h1>
			<h2 style="text-align: center;"><%=session.getAttribute("dLoc")%></h2>
			
			<input type="hidden" name="prac" id="prac" value="<%=session.getAttribute("dPrac")%>"> 
			<input type="hidden" name="loc" id="loc" value="<%=session.getAttribute("dLoc")%>">

			<div style="border: double;">
				<center>
					<table>
						<tr>
							<th><b>Doctor</b></th>
							<th><label><b>Patient</b></label></th>
							<th><label><b>Patient Health Card Number</b></label></th>
							<th><label for="date" id="adate"><b>Date of Appointment</b></label></th>
						</tr>

						<tr>
							<td><input id="dname" name="drname" value="<%=session.getAttribute("Name")%>"></td>
							<%
							try {
								String correctdoc = (String) session.getAttribute("Name");
								String correcthcn = (String) session.getAttribute("pfilehcn");
								Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
								Connection Con = DriverManager.getConnection("jdbc:mysql://localhost:3306/signupinfo", "root",
								"29inWonderlandWoohoo21");
								String patientnames = "SELECT * From patientlist WHERE dName='" + correctdoc + "' AND pHCNUM='" + correcthcn + "'";
								Statement stmtd = Con.createStatement();
								ResultSet patNames = stmtd.executeQuery(patientnames);

								while (patNames.next()) {
									%>
									<td><input name="patname" id="pat" value="<%=patNames.getString("pName")%>"></td>
									<%
								}
							} 
							
							catch (Exception e) {
								e.printStackTrace();
								out.println("Error" + e.getMessage());
							}
							%>
							
							<td><input id="hcn" name="pathcn" value="<%=session.getAttribute("pfilehcn")%>"></td>
							<td><input type="date" name="date" id="adate" required></td>
						</tr>
					</table>
				</center>
			</div>
			<br> <br>
			
			<div class="textcont">
				<center>
					<br> 
					
					<label for="drnotes"><b>Visit Notes / Summary </b></label><br>
					<textarea type="text" name="drnotes" id="drnotes" required></textarea> <br> <br>
					
					<label for="newrx"><b>Diagnosis / Prescriptions (N/A if none)</b></label><br>
					<textarea type="text" name="newrx" required></textarea> <br> <br> 
					
					<label for="refer"><b>Tests Requested / Referrals (N/A if none)</b></label><br>
					<textarea type="text" name="refer" required></textarea> <br> <br> 
					
					<label for="ffUp"><b>Follow Up Scheduled (N/A if none)</b></label></<br>
					
					<textarea type="text" name="ffUp" required></textarea> <br> <br>
				</center>
			</div>
			<br> <br>
			
			<button type="submit" style="background-color: #e1f9e9; padding: 10px;">
			Create New Patient File / Visit Summary
			</button>

		</form>
	</div>
</body>
</html>