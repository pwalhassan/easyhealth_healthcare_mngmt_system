<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<style>
Body {
	font-family: Calibri, Helvetica, sans-serif;
	background-color: rgba(255, 255, 255, 0.2);
	background-image: url("https://i.pinimg.com/originals/1a/ee/5c/1aee5c344846f449350feae457ea350e.jpg");
	background-blend-mode: lighten;
	background-size: 100%;
	background-size: cover; 
    background-position: center center;
    background-repeat: no-repeat;
}

button {
	background-color: #3BCF50;
	width: 100%;
	padding: 7px;
	margin: 15px 0px;
	cursor: pointer;
}

form {
	border: 3px solid #f1f1f1;
}

input[type=text], input[type=password] {
	width: 100%;
	margin: 8px 0;
	padding: 12px 20px;
	display: inline-block;
	border: 2px solid blue;
	box-sizing: border-box;
}

.cancelbtn {
	width: auto;
	padding: 10px 18px;
	margin: 10px 5px;
	background-color: red;
}

.container {
	padding: 25px;
	background-color: lightblue;
}
</style>

	<center>
		<h1>Patient Login Form</h1>
	</center>
	
	<form method = "post" action = "PatientLoginCheck">
		<div class="container">
		
			<label>Username : </label> 
			<input type="text" placeholder="Enter Username" name="Username" required> 
			
			<label>Password : </label> 
			<input type="Password" placeholder="Enter Password" name="Password" required>
			
			<button type="login">Login</button>
			<a href="http://Localhost:8080/EasyHealth/PatientOrDoctor.jsp">
				<button type="button" class="cancelbtn">Cancel</button>
			</a>
			
		</div>
	</form>
</body>
</html>