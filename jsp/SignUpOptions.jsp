<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>SignUpOptions</title>
</head>

<body>
	<style>
Body {
	font-family: Calibri, Helvetica, sans-serif;
	background-color: rgba(255, 255, 255, 0.2);
	background-image:
		url("https://i.pinimg.com/originals/1a/ee/5c/1aee5c344846f449350feae457ea350e.jpg");
	background-blend-mode: lighten;
	background-size: 100%;
	background-size: cover;
	background-position: center center;
	background-repeat: no-repeat;
}

form {
	border: 3px solid #f1f1f1;
}

.container {
	padding: 25px;
	background-color: lightblue;
}

input[type=button] {
	background-color: #e1e2f9;
	width: 100%;
	padding: 7px;
	margin: 15px 0px;
	cursor: pointer;;
}

.cancelbtn {
	width: auto;
	padding: 10px 18px;
	margin: 10px 5px;
	background-color: red;
}
</style>

	<center>
		<h1>Are you a Doctor or a Patient?</h1>
	</center><br>

	<form method="post">
		<div class="container">

			<a href="http://localhost:8080/EasyHealth/PatientSignUp.jsp"> 
				<input type="button" value="Patient" />
			</a> 
			
			<a href="http://localhost:8080/EasyHealth/DoctorSignUp.jsp"> 
				<input type="button" value="Doctor" />
			</a> 
			
			<a href="http://Localhost:8080/EasyHealth/PatientOrDoctor.jsp">
				<button type="button" class="cancelbtn">Cancel</button>
			</a>
			
		</div>
	</form>
</body>
</html>