<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>PatientSignUp</title>
</head>

<body>
	<style>
Body {
	font-family: Calibri, Helvetica, sans-serif;
	background-color: rgba(255, 255, 255, 0.2);
	background-image: url("https://i.pinimg.com/originals/1a/ee/5c/1aee5c344846f449350feae457ea350e.jpg");
	background-blend-mode: lighten;
	background-size: 100%;
	background-size: cover; 
    background-position: center center;
    background-repeat: no-repeat;
}

button {
	background-color: #3BCF50;
	width: 100%;
	padding: 7px;
	margin: 15px 0px;
	cursor: pointer;
}

.resetbttn {
   		width: auto;
   		padding: 10px 18px;
   		margin: 10px 5px;
   		background-color: white;
   }

form {
	border: 3px solid #f1f1f1;
}

input[type=text], input[type=password], input[type=email] {
	width: 100%;
	margin: 8px 0;
	padding: 12px 20px;
	display: inline-block;
	border: 2px solid blue;
	box-sizing: border-box;
}

.container {
	padding: 25px;
	background-color: lightblue;
}
</style>

	<center>
		<h2>Fill out the form below to create your account!</h2>
	</center><br><br>
    
    <form action="createPatient" method="post">
	    <div class="container"> 
		    <label for="pusername">Username :</label><br>
			<input type="text" name="pusername" required><br><br>
			
			<label for="pemail">Email :</label><br>
			<input type="email" name="pemail" required><br><br>
			
			<label for="ppword">Password :</label><br>
			<input type="password" name="ppword" id = "pword" required><br><br>
			
			<label for="prepword">Re-enter Password :</label><br>
			<input type="password" name="prepword" id = "repword" required><br><br><br><br>
			
	    	<label for="fname">First Name :</label><br>
	  		<input type="text" placeholder="First Name" name="fname" required><br><br>
	
	  		<label for="mname">Middle name :</label><br>
	  		<input type="text" placeholder="Middle Name" name="mname"><br><br>
	
	  		<label for="lname">Last Name :</label><br>
	  		<input type="text" placeholder="Last Name" name="lname" required><br><br>
	
			<label for="psex">Sex :</label><br>
	  		<input type="radio" id="male" name="psex" value="Male" required>
			<label for="male">Male</label><br>
			<input type="radio" id="female" name="psex" value="Female">
			<label for="female">Female</label><br><br>
			
			<label for="dob">Date of Birth (DD/MM/YYYY) :</label><br>
			<input type="date" name="dob" required><br><br>
			
			<p>
			Address<br>
			
			<label for="pAddl1">Address Line 1 :</label><br>
			<input type="text" placeholder="1 Your St" name="pAddl1" required><br>
			
			<label for="pAddl2">Address Line 2 :</label><br>
			<input type="text" placeholder="Apt # or PO Box" name="pAddl2"><br>
			
			<label for="city">City/Town :</label><br>
			<input type="text" placeholder="Your City/Town" name="city" required><br>
			
			<label for="prov">State/Province :</label><br>
			<input type="text" placeholder="Your State/Province" name="prov" required><br>
			
			<label for="cntry">Country :</label><br>
			<input type="text" placeholder="Your Country" name="cntry" required><br>
			
			<label for="pcode">Postal Code :</label><br>
			<input type="text" placeholder="Your Postal Code" name="pcode" required><br><br>
			</p>
	
			<label for="hcnum">Health Card Number :</label><br>
			<input type="text" placeholder="000000000000" name="hcnum" required><br><br>
			
			<label for="orgD">Are you an Organ Donor?</label><br>
			<input type="radio" id="yes" name="orgD" value="Yes" required>
			<label for="yes">Yes</label><br>
			<input type="radio" id="no" name="orgD" value="No">
			<label for="no">No</label><br><br>
			
			<button type="submit">Complete My Profile</button>
			<button type="reset" class="resetbttn">Reset</button>
		</div>
  	</form>
</body>
</html>