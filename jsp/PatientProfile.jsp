<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Profile</title>
</head>

<body>
	<style>
.container {
	padding: 25px;
	background-color: lightblue;
}

Body {
	font-family: Calibri, Helvetica, sans-serif;
	background-color: rgba(255, 255, 255, 0.2);
	background-image:
		url("https://i.pinimg.com/originals/1a/ee/5c/1aee5c344846f449350feae457ea350e.jpg");
	background-blend-mode: lighten;
	background-size: 100%;
	background-size: cover;
	background-position: center center;
	background-repeat: no-repeat;
}

ul {
	padding-left: 10px;
	list-style-type: none;
}

ul li {
	padding-left: 40px;
	background-image:
		url("https://files.123freevectors.com/wp-content/uploads/backgroundvectors/abstract-free-medical-background-free-vector.jpg");
	background-size: contain;
	background-repeat: no-repeat;
	font-size: 20px;
}
</style>

	<center>
		<h1>Profile</h1>
	</center>
	
	<form>
		<div class="container">
		
			<ul>
				<li><b>Name: </b><%=session.getAttribute("Name")%></li>
				<br>
				<li><b>Email: </b><%=session.getAttribute("pEmail")%></li>
				<br>
				<li><b>Address: </b><%=session.getAttribute("pAdd1")%></li>
				<br>
				<li><b>Date of Birth: </b><%=session.getAttribute("pDOB")%></li>
				<br>
				<li><b>Sex: </b><%=session.getAttribute("pSex")%></li>
				<br>
				<li><b>Health Card Number: </b><%=session.getAttribute("hcNum")%></li>
				<br>
				<li><b>Organ Donor? : </b><%=session.getAttribute("orgD")%></li>
			</ul>
			
		</div>
	</form>
</body>
</html>