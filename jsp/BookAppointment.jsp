<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Book an appointment</title>
</head>

<body>
	<style>
.container {
	padding: 25px;
	background-color: lightblue;
}

Body {
	font-family: Calibri, Helvetica, sans-serif;
	background-color: rgba(255, 255, 255, 0.2);
	background-image:
		url("https://i.pinimg.com/originals/1a/ee/5c/1aee5c344846f449350feae457ea350e.jpg");
	background-blend-mode: lighten;
	background-size: 100%;
	background-size: cover;
	background-position: center center;
	background-repeat: no-repeat;
}
</style>

	<center>
		<h1>Book an appointment</h1>
	</center>

	<form method="post" action="BookAppointmentServlet">
		<div class="container">

			<label>Doctor Names: </label> <select name="dName" id="docName">
				<option value="-1">Select a Doctor</option>
				<%
				try {
					Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
					Connection Con = DriverManager.getConnection("jdbc:mysql://localhost:3306/signupinfo", "root",
					"29inWonderlandWoohoo21");
					String data = "SELECT * From doctorprofile WHERE dSpec= 'General'";
					Statement stmtd = Con.createStatement();
					ResultSet docNames = stmtd.executeQuery(data);
					int i = 0;

					while (docNames.next()) {
				%>
				<option value="<%=docNames.getString("dFormName")%>"><%=docNames.getString("dFormName")%></option>
				<%
				i = i + 1;
				}
				} catch (Exception e) {
				e.printStackTrace();
				out.println("Error" + e.getMessage());
				}
				%>
			</select><br> <br> <label>Dates:</label> <input type="date"
				name="aDate" required id="aDate"><br> <br>

			<button type="submit">Find Appointment Times</button>

		</div>
	</form>
</body>
</html>