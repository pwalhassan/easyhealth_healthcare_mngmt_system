<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Select Patient</title>
</head>

<body>
	<style>
.container {
	padding: 25px;
	background-color: lightblue;
}

table, th, td, tr {
	border: 1px solid white;
}

th, td, tr {
	background-color: #e1f9e9;
}

button {
	background-color: #e1e2f9;
}

Body {
	font-family: Calibri, Helvetica, sans-serif;
	background-color: rgba(255, 255, 255, 0.2);
	background-image:
		url("https://i.pinimg.com/originals/1a/ee/5c/1aee5c344846f449350feae457ea350e.jpg");
	background-blend-mode: lighten;
	background-size: 100%;
	background-size: cover;
	background-position: center center;
	background-repeat: no-repeat;
}
</style>

	<center>
		<h1>View Your Patients' Files</h1>
	</center>

	<form method="post" action="SelectPatientServlet">
		<div class="container">
		
			<center>
				<table>
					<%
					try {
						String correctDoc = (String) session.getAttribute("Name");
						Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
						Connection Con = DriverManager.getConnection("jdbc:mysql://localhost:3306/signupinfo", "root",
						"29inWonderlandWoohoo21");
						String data = "SELECT * From patientfiles WHERE dName='" + correctDoc + "'";
						Statement stmtd = Con.createStatement();
						ResultSet patNames = stmtd.executeQuery(data);
						List<String> duplicates = new ArrayList<String>();
						
						%><input type="hidden" name="doc" value="<%=correctDoc%>">
						
						<%
						if (patNames.isBeforeFirst()) {
							%>
							<tr>
								<th>Patient Name</th>
								<th>Patient HealthCard Number</th>
								<th>Files To View</th>
							</tr>
							
							<%
							while (patNames.next()) {
								if (duplicates.contains(patNames.getString("pName")) == false) {
									duplicates.add(patNames.getString("pName"));
									%>
									<tr>
										<td><center><%=patNames.getString("pName")%></center></td>
										
										<td><center><%=patNames.getString("pHCN")%></center></td>
										
										<td><center>
											<button value="<%=patNames.getString("pName")%>"name="pnamefile">
											View This Patient's Files
											</button>
										</center></td>
									</tr>
									<%
								}
							}
						}
					} 
					
					catch (Exception e) {
						e.printStackTrace();
						out.println("Error" + e.getMessage());
					}
					%>
					
				</table>
			</center>
		</div>
	</form>
</body>
</html>