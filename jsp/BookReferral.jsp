<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Book Referral</title>
</head>

<body>
	<style>
.container {
	padding: 25px;
	background-color: lightblue;
}

Body {
	font-family: Calibri, Helvetica, sans-serif;
	background-color: rgba(255, 255, 255, 0.2);
	background-image:
		url("https://i.pinimg.com/originals/1a/ee/5c/1aee5c344846f449350feae457ea350e.jpg");
	background-blend-mode: lighten;
	background-size: 100%;
	background-size: cover;
	background-position: center center;
	background-repeat: no-repeat;
}
</style>

	<center>
		<h1>Book an appointment for your referral</h1>
	</center>

	<form method="post" action="ReferralAppointmentServlet">
		<div class="container">
			Doctor Name:
			<%=session.getAttribute("dRefName")%>
			<br> Date:
			<%=session.getAttribute("refApptDate")%>
			<br> <label> Available Times: </label> <select name="appTime">
				<option value="-1">Select a time</option>
				<%
				try {
					Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
					Connection Con = DriverManager.getConnection("jdbc:mysql://localhost:3306/signupinfo", "root",
					"29inWonderlandWoohoo21");
					String data = "SELECT * From appointments WHERE dFName = '" + session.getAttribute("dRefName") + "'";
					Statement stmtt = Con.createStatement();
					ResultSet aTimes = stmtt.executeQuery(data);
					ArrayList<String> takenTimes = new ArrayList<String>();
					ArrayList<String> availableTimes = new ArrayList<String>();
					ArrayList<String> allTimes = new ArrayList<String>(Arrays.asList("8:00 - 9:00", "9:00 - 10:00", "10:00 - 11:00",
					"11:00 - 12:00", "1:00 - 2:00", "2:00 - 3:00", "3:00 - 4:00", "4:00 - 5:00"));

					while (aTimes.next()) {
						takenTimes.add(aTimes.getString("aTime"));
					}

					for (int i = 0; i < allTimes.size(); i++) {
						if (!takenTimes.contains(allTimes.get(i))) {
					availableTimes.add(allTimes.get(i));
						}
					}

					for (int i = 0; i < availableTimes.size(); i++) {
				%>
				<option value="<%=availableTimes.get(i)%>"><%=availableTimes.get(i)%></option>
				<%
				}
				} catch (Exception e) {
				e.printStackTrace();
				out.println("Error" + e.getMessage());
				}
				%>
			</select> <br> <br>

			<button type="submit">Book Appointment</button>

		</div>
	</form>
</body>
</html>