<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>List of Patients</title>
</head>

<body>
	<style>
.container {
	padding: 25px;
	background-color: lightblue;
}

Body {
	font-family: Calibri, Helvetica, sans-serif;
	background-color: rgba(255, 255, 255, 0.2);
	background-image:
		url("https://i.pinimg.com/originals/1a/ee/5c/1aee5c344846f449350feae457ea350e.jpg");
	background-blend-mode: lighten;
	background-size: 100%;
	background-size: cover;
	background-position: center center;
	background-repeat: no-repeat;
}

table, th, td, tr {
	border: 1px solid white;
}

th, td, tr {
	background-color: #e1f9e9;
}

button {
	background-color: #e1e2f9;
}
</style>

	<center>
		<h1>List of Patients</h1>
	</center>

	<div class="container">
	
		<center>
			<form method="post" action="PatientFileInfoServlet">
				<table>
					<%
					try {
						Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
						Connection Con = DriverManager.getConnection("jdbc:mysql://localhost:3306/signupinfo", "root",
						"29inWonderlandWoohoo21");
						String data = "SELECT * From patientlist WHERE dName='" + session.getAttribute("Name") + "'";
						Statement stmtd = Con.createStatement();
						ResultSet patientList = stmtd.executeQuery(data);
						
						if (patientList.isBeforeFirst()) {
							%>
							<tr>
								<th>Patient Name</th>
								<th>Patient HealthCard Number</th>
								<th>Patient Date of Birth</th>
								<th>File Creation</th>
							</tr>
							
							<%
							while (patientList.next()) {
								%>
								<tr>
									<td><center><%=patientList.getString("pName")%></center></td>
									
									<td><center><%=patientList.getString("pHCNum")%></center></td>
									
									<td><center><%=patientList.getString("pDOB")%></center></td>
									
									<td><center>
										<button name="pfilehcn" value="<%=patientList.getString("pHCNum")%>">
										Create New Patient File
										</button>
									</center></td>
								</tr>
								<%
							}
						}
					} 
					
					catch (Exception e) {
						e.printStackTrace();
						out.println("Error" + e.getMessage());
					}
					%>
					
				</table>
			</form>
		</center>
	</div>
</body>
</html>