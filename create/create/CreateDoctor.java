package create;

/**
 * The CreateDoctor class which creates a Doctor object using values entered by a user
 * while attempting to sign up. The information attributes set in this class will be used to 
 * store information about the Doctor in the database and the Doctor Dashboard.
 * 
 */
public class CreateDoctor {
	private String dUser;				// Username
	private String dEmail;				// Email
	private String dPass;				// Password
	private String rePass;				// Re-entered password
	private String fName;				// First name
	private String mName;				// Middle name
	private String lName;				// Last name
	private String sex;					// Sex
	private String dSpecialty;			// Specialty
	private String dLicense;			// License number/code
	private String dPractice;			// Practice name
	private String dLocation;			// Practice location
	private String dFormName;
	
	/** Sets the username
	 * 
	 * @param dUser
	 */
	public void setdUser(String dUser) {
		this.dUser = dUser;
	}

	/** Gets the username
	 * 
	 * @return dUser
	 */
	public String getdUser() {
		return dUser;
	}
	
	/** Sets the email
	 * 
	 * @param dEmail
	 */
	public void setdEmail(String dEmail) {
		this.dEmail = dEmail;
	}
	
	/** Gets the email
	 * 
	 * @return dEmail
	 */
	public String getdEmail() {
		return dEmail;
	}
	
	/** Sets the password
	 * 
	 * @param dPass
	 */
	public void setdPass(String dPass) {
		this.dPass = dPass;
	}
	
	/** Gets the password
	 * 
	 * @return dPass
	 */
	public String getdPass() {
		return dPass;
	}

	/** Sets the re-entered password
	 * 
	 * @param rePass
	 */
	public void setRePass(String rePass) {
		this.rePass = rePass;
	}
	
	/** Gets the re-entered password
	 * 
	 * @return rePass
	 */
	public String getRePass() {
		return rePass;
	}

	/** Sets the first name
	 * 
	 * @param fName
	 */
	public void setdFName(String fName) {
		this.fName = fName;
	}
	
	/** Gets the first name
	 * 
	 * @return fName
	 */
	public String getdFName() {
		return fName;
	}
	
	/** Sets the middle name
	 * 
	 * @param mName
	 */
	public void setdMName(String mName) {
		this.mName = mName;
	}
	
	/** Gets the middle name
	 * 
	 * @return mName
	 */
	public String getdMName() {
		return mName;
	}
	
	/** Sets the last name
	 * 
	 * @param lName
	 */
	public void setdLName(String lName) {
		this.lName = lName;
	}
	
	/** Gets the last name
	 * 
	 * @return lName
	 */
	public String getdLName() {
		return lName;
	}
	
	/** Sets the doctor's sex
	 * 
	 * @param sex
	 */
	public void setdSex(String sex) {
		this.sex = sex;
	}
	
	/** Gets the doctor's sex
	 * 
	 * @return sex
	 */
	public String getdSex() {
		return sex;
	}

	/** Sets the specialty
	 * 
	 * @param dSpecialty
	 */
	public void setdSpecialty(String dSpecialty) {
		this.dSpecialty = dSpecialty;
	}

	/** Gets the specialty
	 * 
	 * @return dSpecialty
	 */
	public String getdSpecialty() {
		return dSpecialty;
	}

	/** Sets the license number/code
	 * 
	 * @param dLicense
	 */
	public void setdLicense(String dLicense) {
		this.dLicense = dLicense;
	}

	/** Gets the license number/code
	 * 
	 * @return dLicense
	 */
	public String getdLicense() {
		return dLicense;
	}

	/** Sets the practice name
	 * 
	 * @param dPractice
	 */
	public void setdPractice(String dPractice) {
		this.dPractice = dPractice;
	}
	
	/** Gets the practice name
	 * 
	 * @return dPractice
	 */
	public String getdPractice() {
		return dPractice;
	}

	/** Sets the practice location
	 * 
	 * @param dLocation
	 */
	public void setdLocation(String dLocation) {
		this.dLocation = dLocation;
	}
	
	/** Gets the practice location
	 * 
	 * @return dLocation
	 */
	public String getdLocation() {
		return dLocation;
	}
	
	/** Sets the formal name of the doctor
	 * 
	 * @param dFormName
	 */
	public void setdFormName(String dFormName) {
		this.dFormName = dFormName;
	}

	/** Gets the formal name of the doctor
	 * 
	 * @return dFormName
	 */
	public String getdFormName() {
		return dFormName;
	}
}
