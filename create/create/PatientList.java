package create;

/**
 * The PatientList class which creates an PatientList object using values taken when a patient is booking 
 * an appointment. The information attributes set in this class will be used to 
 * store information about the patient/doctor in the database.
 * 
 */
public class PatientList {
	private String dName;				// Doctor's name
	private String pName;				// Patient's name
	private String pHCNum;				// Patient's Health Card Number
	private String pDOB;				// Patient's DOB 

	
	/** Sets the Doctor's name
	 * 
	 * @param dName
	 */
	public void setdName(String dName) {
		this.dName = dName;
	}

	/** Gets the Doctor's name
	 * 
	 * @return dName
	 */
	public String getdName() {
		return dName;
	}

	
	/** Sets the patient's name
	 * 
	 * @param pName
	 */
	public void setpName(String pName) {
		this.pName = pName;
	}
	
	/** Gets the patient's name
	 * 
	 * @return pName
	 */
	public String getpName() {
		return pName;
	}

	/** Sets the patient's health card number
	 * 
	 * @param pHCNum
	 */
	public void setpHCNum(String pHCNum) {
		this.pHCNum = pHCNum;
	}
	
	/** Gets the patient's health card number
	 * 
	 * @return pHCNum
	 */
	public String getpHCNum() {
		return pHCNum;
	}	
	
	/** Sets the patient's date of birth
	 * 
	 * @param pDOB
	 */
	public void setpDOB(String pDOB) {
		this.pDOB = pDOB;
	}
	
	/** Gets the patient's date of birth 
	 * 
	 * @return pDOB
	 */
	public String getpDOB() {
		return pDOB;
	}	
}
