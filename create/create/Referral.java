package create;

/**
 * The Referral class which creates a Referral object using values entered by a user
 * while attempting to send a referral. The information attributes set in this class will be used to 
 * store information about the referral in the database.
 * 
 */
public class Referral {
	private String dName;				// Doctor's name
	private String pName;				// Patient's name

	/** Sets the Doctor's name
	 * 
	 * @param dName
	 */
	public void setdName(String dName) {
		this.dName = dName;
	}

	/** Gets the Doctor's name
	 * 
	 * @return dName
	 */
	public String getdName() {
		return dName;
	}

	
	/** Sets the patient's name
	 * 
	 * @param pName
	 */
	public void setpName(String pName) {
		this.pName = pName;
	}
	
	/** Gets the patient's name
	 * 
	 * @return pName
	 */
	public String getpName() {
		return pName;
	}
}