package create;

/** 
 * The CreateFile class which creates a file object using the values entered by a 'Doctor'
 * when the 'Doctor' fills out the Patient File form. This information will be stored in the
 * database using the PatientFileDAO, and will be accessed each time a Doctor or Patient
 * would like to view said Patient's files.
 *
 */
public class CreateFile {
	private String drName;					// Doctor's Name
	private String patName;					// Patient's Name
	private String patHCN;					// Patient's Health Card Number
	private String date;					// Appointment Date
	private String notes;					// Doctor's notes
	private String rxNew;					// Prescriptions & Diagnosis
	private String anyReferral;				// Referrals
	private String followUp;				// Follow Up Appointment
	private String loc;						// Doctor's Practice Location
	private String prac;					// Doctor's Practice Name
	
	/** Set's the doctor's name
	 * 
	 * @param drName
	 */
	public void setDrName(String drName) {
		this.drName = drName;
	}

	/** Get's the doctor's name
	 * 
	 * @return drName
	 */
	public String getDrName() {
		return drName;
	}
	
	/** Set's the patient's name
	 * 
	 * @param patName
	 */
	public void setPatName(String patName) {
		this.patName = patName;
	}

	/** Get's the patient's name
	 * 
	 * @return patName
	 */
	public String getPatName() {
		return patName;
	}
	
	/** Set's the patient's health card number
	 * 
	 * @param patHCN
	 */
	public void setPatHCN(String patHCN) {
		this.patHCN = patHCN;
	}

	/** Get's the patient's health card number
	 * 
	 * @return patHCN
	 */
	public String getPatHCN() {
		return patHCN;
	}

	/** Set's the appointment date
	 * 
	 * @param date
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/** Get's the appointment date
	 * 
	 * @return date
	 */
	public String getDate() {
		return date;
	}
	
	/** Set's the doctor's notes
	 * 
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/** Get's the doctor's notes
	 * 
	 * @return notes
	 */
	public String getNotes() {
		return notes;
	}

	/** Set's the new prescription/diagnosis field
	 * 
	 * @param rxNew
	 */
	public void setRxNew(String rxNew) {
		this.rxNew = rxNew;
	}

	/** Get's the new prescription/diagnosis
	 * 
	 * @return rxNew
	 */
	public String getRxNew() {
		return rxNew;
	}

	/** Set's the referrals field
	 * 
	 * @param anyReferral
	 */
	public void setAnyReferral(String anyReferral) {
		this.anyReferral = anyReferral;
	}

	/** Get's the referrals
	 * 
	 * @return anyReferral
	 */
	public String getAnyReferral() {
		return anyReferral;
	}

	/** Set's the follow up appointment field
	 * 
	 * @param followUp
	 */
	public void setFollowUp(String followUp) {
		this.followUp = followUp;
	}

	/** Get's the follow up appointments
	 * 
	 * @return followUp
	 */
	public String getFollowUp() {
		return followUp;
	}

	/** Set's the practice location
	 * 
	 * @param loc
	 */
	public void setLoc(String loc) {
		this.loc = loc;
	}

	/** Get's the practice location
	 * 
	 * @return loc
	 */
	public String getLoc() {
		return loc;
	}

	/** Set's the practice name
	 * 
	 * @param prac
	 */
	public void setPrac(String prac) {
		this.prac = prac;
	}

	/** Get's the practice name
	 * 
	 * @return prac
	 */
	public String getPrac() {
		return prac;
	}
	
}