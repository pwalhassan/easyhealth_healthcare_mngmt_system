package create;

/**
 * The Appointment class which creates an Appointment object using values entered by a user
 * while attempting to book an appointment. The information attributes set in this class will be used to 
 * store information about the appointment in the database and the Doctor Dashboard/ Patient Dashboards.
 * 
 */
public class Appointment {
	private String dName;				// Doctor's name
	private String pName;				// Patient's name
	private String pHCNum;				// Patient's Health Card Number
	private String aTime;				// Appointment time
	private String aDate;				// Appointment Date
	private String dFName;         		// Doctor's Formal Name

	
	/** Sets the Doctor's name
	 * 
	 * @param dName
	 */
	public void setdName(String dName) {
		this.dName = dName;
	}

	/** Gets the Doctor's name
	 * 
	 * @return dName
	 */
	public String getdName() {
		return dName;
	}
	
	/** Sets the patient's name
	 * 
	 * @param pName
	 */
	public void setpName(String pName) {
		this.pName = pName;
	}
	
	/** Gets the patient's name
	 * 
	 * @return pName
	 */
	public String getpName() {
		return pName;
	}

	/** Sets the patient's health card number
	 * 
	 * @param pHCNum
	 */
	public void setpHCNum(String pHCNum) {
		this.pHCNum = pHCNum;
	}
	
	/** Gets the patient's health card number
	 * 
	 * @return pHCNum
	 */
	public String getpHCNum() {
		return pHCNum;
	}

	/** Sets the appointment time
	 * 
	 * @param aTime
	 */
	public void setaTime(String aTime) {
		this.aTime = aTime;
	}
	
	/** Gets the appointment time
	 * 
	 * @return aTime
	 */
	public String getaTime() {
		return aTime;
	}
	
	/** Sets the appointment date
	 * 
	 * @param aDate
	 */
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}
	
	/** Gets the appointment date
	 * 
	 * @return aDate
	 */
	public String getaDate() {
		return aDate;
	}	
	
	/** Sets the doctor's formal name
	 * 
	 * @param dFName
	 */
	public void setdFName(String dFName) {
		this.dFName = dFName;
	}
	
	/** Gets the doctor's formal name
	 * 
	 * @return dFName
	 */
	public String getdFName() {
		return dFName;
	}	
}
