package create;

/**
 * The CreatePatient class which creates a Patient object using values entered by a user
 * while attempting to sign up. The information attributes set in this class will be used to 
 * store information about the patient in the database and the Patient Dashboard.
 * 
 */
public class CreatePatient {
	
	private String pUser;               // Username
	private String pEmail;				// Email
	private String pPass;				// Password
	private String rePass;				// Re-entered password
	private String fName;				// First name
	private String mName;				// Middle name
	private String lName;				// Last name
	private String sex;					// Sex
	private String dob;					// Date of birth
	private String pAddl1;				// Address Line 1
	private String pAddl2;				// Address Line 2
	private String pCity;				// City
	private String pProv;				// Province
	private String pCntry;				// Country
	private String pPcode;				// Postal code
	private String hcNum;				// Health card number
	private String orgD;				// Organ donor status
	
	/** Sets the username
	 * 
	 * @param pUser
	 */
	public void setpUser(String pUser) {
		this.pUser = pUser;
	}

	/** Gets the username
	 * 
	 * @return pUser
	 */
	public String getpUser() {
		return pUser;
	}
	
	/** Sets the email
	 * 
	 * @param pEmail
	 */
	public void setpEmail(String pEmail) {
		this.pEmail = pEmail;
	}
	
	/** Gets the email
	 * 
	 * @return pEmail
	 */
	public String getpEmail() {
		return pEmail;
	}
	
	/** Sets the password
	 * 
	 * @param pPass
	 */
	public void setpPass(String pPass) {
		this.pPass = pPass;
	}
	
	/** Gets the password
	 * 
	 * @return pPass
	 */
	public String getpPass() {
		return pPass;
	}

	/** Sets the re-entered password
	 * 
	 * @param rePass
	 */
	public void setRePass(String rePass) {
		this.rePass = rePass;
	}
	
	/** Gets the re-entered password
	 * 
	 * @return rePass
	 */
	public String getRePass() {
		return rePass;
	}

	/** Sets the first name
	 * 
	 * @param fName
	 */
	public void setpFName(String fName) {
		this.fName = fName;
	}
	
	/** Gets the first name
	 * 
	 * @return fName
	 */
	public String getpFName() {
		return fName;
	}
	
	/** Sets the middle name
	 * 
	 * @param mName
	 */
	public void setpMName(String mName) {
		this.mName = mName;
	}
	
	/** Gets the middle name
	 * 
	 * @return mName
	 */
	public String getpMName() {
		return mName;
	}
	
	/** Sets the last name
	 * 
	 * @param lName
	 */
	public void setpLName(String lName) {
		this.lName = lName;
	}
	
	/** Gets the last name
	 * 
	 * @return lName
	 */
	public String getpLName() {
		return lName;
	}
	
	/** Sets the patient's sex
	 * 
	 * @param sex
	 */
	public void setpSex(String sex) {
		this.sex = sex;
	}
	
	/** Gets the patient's sex
	 * 
	 * @return sex
	 */
	public String getpSex() {
		return sex;
	}
	
	/** Sets the date of birth
	 * 
	 * @param dob
	 */
	public void setpDOB(String dob) {
		this.dob = dob;
	}
	
	/** Gets the date of birth
	 * 
	 * @return dob
	 */
	public String getpDOB() {
		return dob;
	}
	
	/** Sets the address line 1
	 * 
	 * @param pAddl1
	 */
	public void setpAddl1(String pAddl1) {
		this.pAddl1 = pAddl1;
	}
	
	/** Gets the address line 2
	 * 
	 * @return pAddl1
	 */
	public String getpAddl1() {
		return pAddl1;
	}
	
	/** Sets the address line 2
	 * 
	 * @param pAddl2
	 */
	public void setpAddl2(String pAddl2) {
		this.pAddl2 = pAddl2;
	}
	
	/** Gets the address line 2
	 * 
	 * @return pAddl2
	 */
	public String getpAddl2() {
		return pAddl2;
	}
	
	/** Sets the city
	 * 
	 * @param pCity
	 */
	public void setpCity(String pCity) {
		this.pCity = pCity;
	}
	
	/** Gets the city
	 * 
	 * @return pCity
	 */
	public String getpCity() {
		return pCity;
	}
	
	/** Sets the province
	 * 
	 * @param pProv
	 */
	public void setpProv(String pProv) {
		this.pProv = pProv;
	}
	
	/** Gets the province
	 * 
	 * @return pProv
	 */
	public String getpProv() {
		return pProv;
	}

	/** Sets the country
	 * 
	 * @param pCntry
	 */
	public void setpCntry(String pCntry) {
		this.pCntry = pCntry;
	}
	
	/** Gets the country
	 * 
	 * @return pCntry
	 */
	public String getpCntry() {
		return pCntry;
	}

	/** Sets the postal code
	 * 
	 * @param pPcode
	 */
	public void setpPcode(String pPcode) {
		this.pPcode = pPcode;
	}
	
	/** Gets the postal code
	 * 
	 * @return pPcode
	 */
	public String getpPcode() {
		return pPcode;
	}

	/** Sets the health card number
	 * 
	 * @param hcNum
	 */
	public void setHCNum(String hcNum) {
		this.hcNum = hcNum;
	}
	
	/** Gets the health card number
	 * 
	 * @return hcNum
	 */
	public String getHCNum() {
		return hcNum;
	}
	
	/** Sets the organ donor status
	 * 
	 * @param orgD
	 */
	public void setOrgD(String orgD) {
		this.orgD = orgD;
	}
	
	/** Gets the organ donor status
	 * 
	 * @return orgD
	 */
	public String getOrgD() {
		return orgD;
	}
}
