# Healthcare Management System

The Healthcare Management System would be a web based application that manages the interactions between patients, doctors and administration. Patients can register a profile, log in, view their files, and book or cancel an appointment with a general practitioner. Doctors can register a profile and login, can view and edit patients files, refer patients to specialists, cancel appointments and view their upcoming appointments. The administration component would manage the appointments made, keep track of the list of patients and doctors, and calculate invoices for patients and private practices. This system would help reduce wait times and save time for both the doctor and the patient.


## IMPLEMENTATION

### Patient Profile                                              
- Name
- Sex
- Healthcard Number 
- Email 
- Date of Birth 
- Address 
- Family Medical History
- Organ Donor 
- File

### Doctor Profile
- Name
- Email
- Sex
- Specialty
- License Number
- Practice
- Location
- List of Patients


## SOFTWARE TOOLS
- Java
- IDE to code in
- Database
- Server
