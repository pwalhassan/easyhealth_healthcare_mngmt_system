package servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import database.DBConnection;

import java.sql.ResultSet;

/**
 * The PatientLoginCheck servlet which queries the database for the information entered while
 * attempting to log in as a doctor.
 * 
 */
public class PatientLoginCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Getting the username and password info from the login page
		String Username = request.getParameter("Username");
		String Password = request.getParameter("Password");

		// Establishing a connection to the database in order to check if the login information matches for the user
		try {
			Connection cnnct = DBConnection.connect();
			
			// Creating statement objects which will send the sql commands for checking the login info to the database
			Statement stmtUP = cnnct.createStatement();
			Statement stmtU = cnnct.createStatement();
			Statement stmtD = cnnct.createStatement();
			
			// The sql commands to be executed using the created statement objects
			String checkUser = "SELECT * FROM patientprofile WHERE pUser='" + Username + "'";
			String checkUnP = "SELECT * FROM patientprofile WHERE pUser='" + Username + "' and pPass='" + Password + "'";
			String Data = "SELECT * FROM patientprofile WHERE pUser='" + Username + "'";
			
			// Executing the sql commands with the statement objects
			ResultSet user = stmtU.executeQuery(checkUser);
			ResultSet userPass = stmtUP.executeQuery(checkUnP);
			ResultSet pData = stmtD.executeQuery(Data);
			
			String PatName = "";
			String PatEmail = "";
			String PatSex = "";
			String PatDOB = "";
			String PatAddress = "";
			String PatHCNum = "";
			String PatOrgD = "";
			
			
			if(pData.next())
			{
				PatSex = pData.getString("pSex");
				PatName = pData.getString("pName");
				PatEmail = pData.getString("pEmail");
				PatDOB = pData.getString("pDOB");
				PatAddress = pData.getString("pAdd1");
				PatHCNum = pData.getString("pHCNum");
				PatOrgD = pData.getString("pOrgD");
			}
	
			// Logs the user in if the information from the database matches the username and password entered
			if (userPass.next()) {
				HttpSession session = request.getSession();
				session.setAttribute("pEmail", PatEmail);
				session.setAttribute("Name", PatName);
				session.setAttribute("pDOB", PatDOB);
				session.setAttribute("pAdd1", PatAddress);
				session.setAttribute("hcNum", PatHCNum);
				session.setAttribute("orgD", PatOrgD);
				session.setAttribute("pSex", PatSex);

				response.sendRedirect("http://localhost:8080/EasyHealth/PatientDashboard.jsp");
			} 
			
			else {
				
				// Alerts the user to check the password if only the username matches
				if (user.next()) {
					PrintWriter out = response.getWriter();
					out.println("<script type=\"text/javascript\">");
					out.println("alert('Password does not match!');");
					out.println("location='http://localhost:8080/EasyHealth/PatientLogin.jsp';");
					out.println("</script>");
				}
				
				// Alerts the user that the username does not exist in the database
				else {
					PrintWriter out = response.getWriter();
					out.println("<script type=\"text/javascript\">");
					out.println("alert('Username does not exist!');");
					out.println("location='http://localhost:8080/EasyHealth/PatientLogin.jsp';");
					out.println("</script>");
				}
			}
			
			// Closing all the connections opened
			try {
				user.close();
				userPass.close();
				stmtU.close();
				stmtUP.close();
			} 
			
			catch (SQLException e) {
				e.printStackTrace();
			}
		}

		// Catching any exceptions thrown while trying to establish the connection with the database
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
