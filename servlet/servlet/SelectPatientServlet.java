package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import database.DBConnection;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * The SelectPatientServlet class which gets the Patient information from the patient selection page
 * from which a doctor can select which Patient's files they would like to view.
 */
public class SelectPatientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Getting the patient name and doctor's name from the previous web page
		String patient = request.getParameter("pnamefile");
		String doc = request.getParameter("doc");
		
		try {
			Connection cnnct = DBConnection.connect();
			
			Statement stmtpat = cnnct.createStatement();
			
			String findFiles = "SELECT * FROM patientfiles WHERE pName='" + patient + "' AND dName='" + doc +"'";
			
			// Executing the sql commands with the statement objects
			ResultSet patientr = stmtpat.executeQuery(findFiles);
			int numFiles = 0;

			List<String> pNames = new ArrayList<String>();
			List<String> dates = new ArrayList<String>();
			
			// Adding all files of the selected patient to the pNames and dates lists in case there are multiple
			// files for this patient
			while(patientr.next()) {
				pNames.add(patientr.getString("pName"));
				dates.add(patientr.getString("apptDate"));
				numFiles = numFiles + 1;
				
			}

			// Setting the information from above to an HTTP session to be accessed by the select file web page
			if (numFiles != 0) {
				HttpSession sessionpf = request.getSession();
				sessionpf.setAttribute("numFiles", numFiles);
				sessionpf.setAttribute("pnames", pNames);
				sessionpf.setAttribute("dates", dates);
				sessionpf.setAttribute("docn", doc);
				response.sendRedirect("http://localhost:8080/EasyHealth/SelectFile.jsp");
				
			}
			
			// If the selected patient does not have any files yet
			else {
				PrintWriter out = response.getWriter();
				out.println("<script type=\"text/javascript\">");
				out.println("location='http://localhost:8080/EasyHealth/DoctorDashboard.jsp';");
				out.println("alert('No Files Available!');");
				out.println("</script>");
			}
		}
		
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
