package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import database.DBConnection;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * The BookAppointmentServlet class which will get the booking information when a patient attempts to book
 * an appointment with a doctor. The information collected by this servlet will be subsequently stored in the database.
 */
public class BookAppointmentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Getting the doctor's name and appointment date values entered in the appointment booking web page
		String docFormName = request.getParameter("dName");
		String aDate = request.getParameter("aDate");
		
		try {
			Connection cnnt = DBConnection.connect();
			Statement stmtdn = cnnt.createStatement();
			String findDName = "SELECT * FROM doctorprofile WHERE dFormName='" + docFormName + "'";
			ResultSet Doctor = stmtdn.executeQuery(findDName);
			String dName = "";
			
			while(Doctor.next()) {
				dName = Doctor.getString("dName");
			}
			
			HttpSession session = request.getSession();
			session.setAttribute("aDName", dName);
			session.setAttribute("aDFName", docFormName);
			session.setAttribute("aDate", aDate);
			
			response.sendRedirect("http://localhost:8080/EasyHealth/AppointmentTimes.jsp");
		}
		
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
