package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import database.DBConnection;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * The CancelAppointmentD servlet class to be used to process an appointment cancellation by a Doctor.
 * The appointment to be cancelled will be deleted from the database.
 */
public class CancelAppointmentD extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Getting the appointment to be canceled from the cancel appointment webpage
		String apptInfo = request.getParameter("CancelAppt");
		int deleted = 0;
		
		String[] info = apptInfo.split("/");
		String aDate = info[0];
		String aTime = info[1];
		String pName = info[2];

		HttpSession session = request.getSession();
		
		try {
			Connection cnnt = DBConnection.connect();
			
			String appt = "DELETE FROM appointments WHERE dName ='" + session.getAttribute("Name") + "' and aTime = '" 
					      + aTime + "' and aDate = '" + aDate + "' and pName = '" + pName + "'";
			PreparedStatement stat = cnnt.prepareStatement(appt);
			deleted = stat.executeUpdate();
			
			response.sendRedirect("http://localhost:8080/EasyHealth/DoctorDashboard.jsp");
		}
		
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
