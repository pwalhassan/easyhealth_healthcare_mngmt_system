package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import database.DBConnection;

/**
 * The DoctorLoginCheck servlet which queries the database for the information entered while
 * attempting to log in as a doctor.
 * 
 */
public class DoctorLoginCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Getting the username and password info from the login page
		String Username = request.getParameter("username");
		String Password = request.getParameter("password");

		// Establishing a connection to the database in order to check if the login information matches for the user
		try {
			Connection cnnct = DBConnection.connect();
			
			// Creating statement objects which will send the sql commands for checking the login info to the database
			Statement stmtUP = cnnct.createStatement();
			Statement stmtU = cnnct.createStatement();
			Statement stmtD = cnnct.createStatement();
			
			// The sql commands to be executed using the created statement objects
			String checkUser = "SELECT * FROM doctorprofile WHERE dUser='" + Username + "'";
			String checkUnP = "SELECT * FROM doctorprofile WHERE dUser='" + Username + "' and dPass='" + Password
					+ "'";
			String dData = "SELECT * FROM doctorprofile WHERE dUser='" + Username + "'";
			
			// Executing the sql commands with the statement objects
			ResultSet user = stmtU.executeQuery(checkUser);
			ResultSet userPass = stmtUP.executeQuery(checkUnP);
			ResultSet Data = stmtD.executeQuery(dData);

			String DocName = "";
			String DocEmail = "";
			String DocSex = "";
			String DocLoc = "";
			String DocPrac = "";
			String DocSpec = "";
			String DocLicNum = "";
			String DocFormName= "";
			
			// If the doctor exists in the database, get the information from the database
			if(Data.next()) {
				DocSex = Data.getString("dSex");
				DocName = Data.getString("dName");
				DocEmail = Data.getString("dEmail");
				DocLoc = Data.getString("dLoc");
				DocPrac = Data.getString("dPrac");
				DocSpec = Data.getString("dSpec");
				DocLicNum = Data.getString("dLicNum");
				DocFormName = Data.getString("dFormName");
			}

			// If the username and password match, set the information to an HTTP session to be accessed by another page
			if (userPass.next()) {
				HttpSession session1 = request.getSession();
				session1.setAttribute("dEmail", DocEmail);
				session1.setAttribute("Name", DocName);
				session1.setAttribute("dSex", DocSex);
				session1.setAttribute("dLoc", DocLoc);
				session1.setAttribute("dLicNum", DocLicNum);
				session1.setAttribute("dPrac", DocPrac);
				session1.setAttribute("dSpec", DocSpec);
				session1.setAttribute("dFormName", DocFormName);
				
				response.sendRedirect("http://localhost:8080/EasyHealth/DoctorDashboard.jsp");
			} 
			
			else {
				
				// Alerts the user to check the password if only the username matches
				if (user.next()) {
					PrintWriter out = response.getWriter();
					out.println("<script type=\"text/javascript\">");
					out.println("alert('Password does not match!');");
					out.println("location='http://localhost:8080/EasyHealth/DoctorLogin.jsp';");
					out.println("</script>");
				} 
				
				// Alerts the user that the username does not exist in the database
				else {
					PrintWriter out = response.getWriter();
					out.println("<script type=\"text/javascript\">");
					out.println("alert('Username does not exist!');");
					out.println("location='http://localhost:8080/EasyHealth/DoctorLogin.jsp';");
					out.println("</script>");
				}
			}
			
			// Closing all the connections opened
			try {
				user.close();
				userPass.close();
				stmtU.close();
				stmtUP.close();
			} 
			
			// Catching any exceptions thrown while trying to close the connections
			catch (SQLException e) {
				e.printStackTrace();
			}
		}

		// Catching any exceptions thrown while trying to establish the connection with the database
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
