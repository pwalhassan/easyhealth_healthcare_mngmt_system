package servlet;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * The BookReferralServlet class which will be used to get the referral appointment booking information 
 * from it's web page. The information collected by the servlet will be subsequently stored in the database.
 */
public class BookReferralServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Getting the information entered in the referral booking web page
		String dName = request.getParameter("dName");
		String refApptTime = request.getParameter("refDate");
		
		// Setting the relevant information in an HTTP session to be accessed later by another web page
		HttpSession session = request.getSession();
		session.setAttribute("dRefName", dName);
		session.setAttribute("refApptDate", refApptTime);
		
		response.sendRedirect("http://localhost:8080/EasyHealth/BookReferral.jsp");
	}

}
