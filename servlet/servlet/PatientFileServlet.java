package servlet;

import java.io.IOException;

import create.CreateFile;
import database.PatientFileDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * The PatientFileServlet class to be used to get the file information from the file creation web page.
 * The information will beb subsequently stored in the database, to be accessed when the Patient or Doctor
 * would like to view the file.
 */
public class PatientFileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    private PatientFileDAO FileDAO;
	
	public void init() {
		FileDAO = new PatientFileDAO();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Value to check that the file info has been stored successfully
		int fileAdd = 0;

		// Getting the information entered in each section of the file creation form using 
		// the parameter names from the form
		String loc = request.getParameter("loc");
		String prac = request.getParameter("prac");
		String dName = request.getParameter("drname");
		String pName = request.getParameter("patname");
		String pHCN = request.getParameter("pathcn");
		String apptDate = request.getParameter("date");
		String drNotes = request.getParameter("drnotes");
		String newRx = request.getParameter("newrx");
		String referral = request.getParameter("refer");
		String ffUp = request.getParameter("ffUp");
	
		// Creating a new instance of a File object to set the patient file info
		CreateFile newFile = new CreateFile();

		// Setting the file information for the Patient File object
		newFile.setLoc(loc);
		newFile.setPrac(prac);
		newFile.setDrName(dName);
		newFile.setPatName(pName);
		newFile.setDate(apptDate);
		newFile.setPatHCN(pHCN);
		newFile.setNotes(drNotes);
		newFile.setRxNew(newRx);
		newFile.setAnyReferral(referral);
		newFile.setFollowUp(ffUp);

		// Storing the file information in the database
		try {
			fileAdd = FileDAO.StoreFile(newFile);
		} 
		
		// Catching any exceptions thrown while trying to store the information
		catch (Exception e) {
			e.printStackTrace();
		}
		
		// Sending the doctor back to the main dashboard if the files has been created successfully
		if (fileAdd > 0) {
			try {
				response.sendRedirect("http://localhost:8080/EasyHealth/DoctorDashboard.jsp");
			}
			
			// Catching any exceptions thrown while trying to set the session attributes
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
