package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import database.DBConnection;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * The CancelAppointmentP servlet class to be used to process an appointment cancellation by a Patient.
 * The appointment to be cancelled will be deleted from the database.
 */
public class CancelAppointmentP extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Getting the appointment to be cancelled from the cancel appointment web page
		String apptInfo = request.getParameter("CancelAppt");
		int deleted = 0;
		
		String[] info = apptInfo.split("/");
		String aDate = info[0];
		String aTime = info[1];
		String dName = info[2];

		try {
			Connection cnnt = DBConnection.connect();
			
			String appt = "DELETE FROM appointments WHERE dFName ='" + dName + "' and aTime = '" + aTime 
						  + "' and aDate = '" + aDate + "'";
			PreparedStatement stat = cnnt.prepareStatement(appt);
			deleted = stat.executeUpdate();
			response.sendRedirect("http://localhost:8080/EasyHealth/PatientDashboard.jsp");
		}
		
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
