package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import database.DBConnection;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * The SelectPatientServlet class which will be used in the file viewing process. It will get all files
 * for the requested patient from the database, which will be sent to another web page to be viewed individually.
 */
public class GetPatientFilesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Getting the patient name from the previous web page
		String patient = request.getParameter("pnamefile");
		
		try {
			Connection cnnct = DBConnection.connect();
			
			Statement stmtpat = cnnct.createStatement();
			
			String findFiles = "SELECT * FROM patientfiles WHERE pName='" + patient + "'";
			
			// Executing the sql commands with the statement objects
			ResultSet patientr = stmtpat.executeQuery(findFiles);
			int numFilesP = 0;

			List<String> pNames = new ArrayList<String>();
			List<String> dates = new ArrayList<String>();
			
			// Adding the patient name and appointment date to the pNames and dates lists in case
			// there is more than one file in the database for this particular patient
			while(patientr.next()) {
				pNames.add(patientr.getString("pName"));
				dates.add(patientr.getString("apptDate"));
				numFilesP = numFilesP + 1;
				
			}

			// Setting the files information to an HTTP session to be accessed by the select file web page
			if (numFilesP != 0) {
				HttpSession sessionpf = request.getSession();
				sessionpf.setAttribute("numFiles", numFilesP);
				sessionpf.setAttribute("pnames", pNames);
				sessionpf.setAttribute("dates", dates);
				response.sendRedirect("http://localhost:8080/EasyHealth/SelectFilePatient.jsp");
				
			}
			
			// If there aren't any files available for the patient
			else {
				PrintWriter out = response.getWriter();
				out.println("<script type=\"text/javascript\">");
				out.println("location='http://localhost:8080/EasyHealth/PatientDashboard.jsp';");
				out.println("alert('No Files Available!');");
				out.println("</script>");
			}
		}
		
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
