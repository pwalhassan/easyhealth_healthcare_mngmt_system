package servlet;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * The PatientFileInfoServlet class which will be used to get the information of the patient for which a file
 * is about to be created.
 */
public class PatientFileInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Getting the patient's name and health card number from the previous web page
		String pfilename = request.getParameter("pfilename");
		String pfilehcn = request.getParameter("pfilehcn");
		
		// Setting this information to the patient file creation web page
		try {
			HttpSession session = request.getSession();
			session.setAttribute("pfilename", pfilename);
			session.setAttribute("pfilehcn", pfilehcn);
			response.sendRedirect("http://localhost:8080/EasyHealth/PatientFile.jsp");
		}
		
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
