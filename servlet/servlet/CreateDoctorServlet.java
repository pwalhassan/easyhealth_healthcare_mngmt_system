package servlet;

import jakarta.servlet.ServletException;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;


import create.CreateDoctor;
import database.DBConnection;
import database.DoctorDAO;

/**
 * The CreateDoctorServlet which extracts information from the doctor sign up page
 * when a user enters any information and attempts to sign up. The information collected
 * from the sign up page using the servlet will be subsequently stored in the database.
 * 
 */
public class CreateDoctorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	// The doctor data access object which will be used to store the info collected
	// into the database
	private DoctorDAO doctorDAO;
	
	public void init() {
		doctorDAO = new DoctorDAO();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Value to check that the profile info has been stored successfully
		int dProfile = 0;

		// Getting the information entered in each section of the sign up form using the parameter names from the form
		String dEmail = request.getParameter("demail");
		String dUser = request.getParameter("dusername");
		String dPass = request.getParameter("dpword");
		String dRepass = request.getParameter("drepword");
		String firstName = request.getParameter("fname");
		String lastName = request.getParameter("lname");
		String midName = request.getParameter("mname");
		String dSex = request.getParameter("dsex");
		String dSpec = request.getParameter("dspec");
		String dLicNum = request.getParameter("dlicnum");
		String dPrac = request.getParameter("dprac");
		String dLoc = request.getParameter("dloc");

		// Using the Doctor object and the set values to store data in the database, using the doctor DAO
		try {
			Connection cnnct = DBConnection.connect();
			Statement stmtU = cnnct.createStatement();
			Statement stmtL = cnnct.createStatement();
			
			String checkUser = "SELECT * FROM doctorprofile WHERE dUser='" + dUser + "'";
			String checkLicNum = "SELECT * FROM doctorprofile WHERE dLicNum='" + dLicNum + "'";
			
			ResultSet dData = stmtU.executeQuery(checkUser);
			ResultSet dLN = stmtL.executeQuery(checkLicNum);
			
			// Checking to see if the username entered is already in the database
			if(dData.next()) {
				PrintWriter out = response.getWriter();
				out.println("<script type=\"text/javascript\">");
				out.println("alert('Username is already taken!');");
				out.println("location='http://localhost:8080/EasyHealth/DoctorSignUp.jsp';");
				out.println("</script>");
			}
			
			// Checking to see if the license number entered is already in the database
			else if(dLN.next()) {
				PrintWriter out = response.getWriter();
				out.println("<script type=\"text/javascript\">");
				out.println("alert('This License Number already has an account! Try logging in.');");
				out.println("location='http://localhost:8080/EasyHealth/DoctorLogin.jsp';");
				out.println("</script>");
			}
			
			else {
				// Creating a new Doctor object if all checks have passed
				if(dPass.equals(dRepass)) {
					CreateDoctor newDoctor = new CreateDoctor();
					String dFormName = "Dr." + " " + lastName;
					
					newDoctor.setdUser(dUser);
					newDoctor.setdEmail(dEmail);
					newDoctor.setdPass(dPass);
					newDoctor.setRePass(dRepass);
					newDoctor.setdFName(firstName);
					newDoctor.setdMName(midName);
					newDoctor.setdLName(lastName);
					newDoctor.setdSex(dSex);
					newDoctor.setdSpecialty(dSpec);
					newDoctor.setdLicense(dLicNum);
					newDoctor.setdPractice(dPrac);
					newDoctor.setdLocation(dLoc);
					newDoctor.setdFormName(dFormName);

					// Attempting to store the information in the database
					try {
						dProfile = doctorDAO.StoreDoctor(newDoctor);
					} 
					
					catch (Exception e) {
						e.printStackTrace();
					}
					
					// Setting the information to an HTTP session to be accessed later by other webpages
					if (dProfile > 0) {
						HttpSession session = request.getSession();
						session.setAttribute("dEmail", newDoctor.getdEmail());
						session.setAttribute("dUser", newDoctor.getdUser());
						session.setAttribute("dPass", newDoctor.getdPass());
						session.setAttribute("dRepass", newDoctor.getRePass());
						session.setAttribute("firstName", newDoctor.getdFName());
						session.setAttribute("midName", newDoctor.getdMName());
						session.setAttribute("lastName", newDoctor.getdLName());
						session.setAttribute("dSex", newDoctor.getdSex());
						session.setAttribute("dSpec", newDoctor.getdSpecialty());
						session.setAttribute("dLicNum", newDoctor.getdLicense());
						session.setAttribute("dPrac", newDoctor.getdPractice());
						session.setAttribute("dLoc", newDoctor.getdLocation());
						session.setAttribute("Name", newDoctor.getdFName() + " " + newDoctor.getdMName() 
											+ " " + newDoctor.getdLName());

						response.sendRedirect("http://localhost:8080/EasyHealth/DoctorDashboard.jsp");
					}
				}
				
				// Checking if the 'password' and 're-enter password' fields don't match
				else {
					PrintWriter out = response.getWriter();
					out.println("<script type=\"text/javascript\">");
					out.println("alert('Passwords do not match!');");
					out.println("location='http://localhost:8080/EasyHealth/DoctorSignUp.jsp';");
					out.println("</script>");
				}
			}
		}
		
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
