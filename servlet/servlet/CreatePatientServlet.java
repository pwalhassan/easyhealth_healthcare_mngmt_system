package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import create.CreatePatient;
import database.DBConnection;
import database.PatientDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * The CreatePatientServlet which extracts information from the doctor sign up page
 * when a user enters any information and attempts to sign up. The information collected
 * from the sign up page using the servlet will be subsequently stored in the database.
 * 
 */
public class CreatePatientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	// The patient data access object which will be used to store the info collected
	// into the database
	private PatientDAO patientDAO;
	
	public void init() {
		patientDAO = new PatientDAO();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// Value to check that the profile info has been stored successfully
		int pProfile = 0;

		// Getting the information entered in each section of the sign up form using the parameter names from the form
		String pEmail = request.getParameter("pemail");
		String pUser = request.getParameter("pusername");
		String pPass = request.getParameter("ppword");
		String pRepass = request.getParameter("prepword");
		String firstName = request.getParameter("fname");
		String lastName = request.getParameter("lname");
		String midName = request.getParameter("mname");
		String pSex = request.getParameter("psex");
		String pDOB = request.getParameter("dob");
		String pAddl1 = request.getParameter("pAddl1");
		String pAddl2 = request.getParameter("pAddl2");
		String pCity = request.getParameter("city");
		String pProv = request.getParameter("prov");
		String pCntry = request.getParameter("cntry");
		String pPostal = request.getParameter("pcode");
		String hcNum = request.getParameter("hcnum");
		String orgD = request.getParameter("orgD");

		// Using the Patient object and the set values to store data in the database, using the patient DAO
		try {
			Connection cnnct = DBConnection.connect();
			Statement stmtU = cnnct.createStatement();
			Statement stmtH = cnnct.createStatement();
			
			String checkUser = "SELECT * FROM patientprofile WHERE pUser='" + pUser + "'";
			String checkHCNum = "SELECT * FROM patientprofile WHERE pHCNum='" + hcNum + "'";
			
			ResultSet pData = stmtU.executeQuery(checkUser);
			ResultSet pHN = stmtH.executeQuery(checkHCNum);
			
			// Checking if the username entered is already in the database
			if(pData.next()) {
				PrintWriter out = response.getWriter();
				out.println("<script type=\"text/javascript\">");
				out.println("alert('Username is already taken!');");
				out.println("location='http://localhost:8080/EasyHealth/PatientSignUp.jsp';");
				out.println("</script>");
			}
			
			// Checking if the health card number entered is already in the database
			else if(pHN.next()) {
				PrintWriter out = response.getWriter();
				out.println("<script type=\"text/javascript\">");
				out.println("alert('This Healthcard Number already has an account! Try logging in.');");
				out.println("location='http://localhost:8080/EasyHealth/PatientLogin.jsp';");
				out.println("</script>");
			}
			
			else {
				if(pPass.equals(pRepass)) {
					// Creating a new Patient object if all checks have passed
					if(hcNum.length() == 12) {
						CreatePatient newPatient = new CreatePatient();

						newPatient.setpUser(pUser);
						newPatient.setpEmail(pEmail);
						newPatient.setpPass(pPass);
						newPatient.setRePass(pRepass);
						newPatient.setpFName(firstName);
						newPatient.setpMName(midName);
						newPatient.setpLName(lastName);
						newPatient.setpSex(pSex);
						newPatient.setpDOB(pDOB);
						newPatient.setpAddl1(pAddl1);
						newPatient.setpAddl2(pAddl2);
						newPatient.setpCity(pCity);
						newPatient.setpProv(pProv);
						newPatient.setpCntry(pCntry);
						newPatient.setpPcode(pPostal);
						newPatient.setHCNum(hcNum);
						newPatient.setOrgD(orgD);

						// Attempting to store the patient information into the database
						try {
							pProfile = patientDAO.StorePatient(newPatient);
						} 
						
						catch (Exception e) {
							e.printStackTrace();
						}
						
						if (pProfile > 0) {
							String Address = "";
							
							if (newPatient.getpAddl2() == "") {
								Address = newPatient.getpAddl1() + ", " + newPatient.getpCity() + ", "
										+ newPatient.getpProv() + ", " + newPatient.getpCntry() +", " 
										+ newPatient.getpPcode();
							}
							else {
								Address = newPatient.getpAddl1() + ", " + newPatient.getpAddl2() + ", " 
										+ newPatient.getpCity() + ", " + 
										newPatient.getpProv() + ", " + newPatient.getpCntry() +", " 
										+ newPatient.getpPcode();
							}
							
							// Setting the information in an HTTP session to be accessed later by other web pages
							HttpSession session = request.getSession();
							session.setAttribute("pEmail", newPatient.getpEmail());
							session.setAttribute("pUser", newPatient.getpUser());
							session.setAttribute("pPass", newPatient.getpPass());
							session.setAttribute("pRepass", newPatient.getRePass());
							session.setAttribute("firstName", newPatient.getpFName());
							session.setAttribute("midName", newPatient.getpMName());
							session.setAttribute("lastName", newPatient.getpLName());
							session.setAttribute("pSex", newPatient.getpSex());
							session.setAttribute("pDOB", newPatient.getpDOB());
							session.setAttribute("pAddl1", newPatient.getpAddl1());
							session.setAttribute("pAddl2", newPatient.getpAddl2());
							session.setAttribute("pCity", newPatient.getpCity());
							session.setAttribute("pProv", newPatient.getpProv());
							session.setAttribute("pCntry", newPatient.getpCntry());
							session.setAttribute("pPostal", newPatient.getpPcode());
							session.setAttribute("hcNum", newPatient.getHCNum());
							session.setAttribute("orgD", newPatient.getOrgD());
							session.setAttribute("Name", newPatient.getpFName() + " " + newPatient.getpMName() 
												+ " " + newPatient.getpLName());
							session.setAttribute("pAdd1", Address);
							
							response.sendRedirect("http://localhost:8080/EasyHealth/PatientDashboard.jsp");
						}
					}
					
					// If the health card number is not 12 digits long
					else {
						PrintWriter out = response.getWriter();
						out.println("<script type=\"text/javascript\">");
						out.println("alert('Your healthcard number is the wrong length!');");
						out.println("location='http://localhost:8080/EasyHealth/PatientSignUp.jsp';");
						out.println("</script>");
					}
				}
				
				// If the 'password' and 're-entered password' fields do not match
				else
				{
					PrintWriter out = response.getWriter();
					out.println("<script type=\"text/javascript\">");
					out.println("alert('Passwords do not match!');");
					out.println("location='http://localhost:8080/EasyHealth/PatientSignUp.jsp';");
					out.println("</script>");
				}
			}
		}
		
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
