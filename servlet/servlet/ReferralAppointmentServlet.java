package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import create.Appointment;
import create.PatientList;
import database.AppointmentDAO;
import database.DBConnection;
import database.PatientListDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * The ReferralAppointmentServlet class to be used by a patient to create an appointment with a
 * doctor they have a referral to. The appointment information will be stored in the database, and the
 * referral will be deleted from the database.
 */
public class ReferralAppointmentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private AppointmentDAO appointmentDAO;
	private PatientListDAO PatientListDAO;
	
	public void init() {
		appointmentDAO = new AppointmentDAO();
		PatientListDAO = new PatientListDAO();
	}
    
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int app = 0;
		int PL = 0;
		int stored = 0;
		
		// Getting the appointment information from the previous web page
		HttpSession session = request.getSession();
		String PatName = (String) session.getAttribute("Name");
		String DocFName = (String) session.getAttribute("dRefName");
		String aTime = request.getParameter("appTime");
		String aDate = (String) session.getAttribute("refApptDate");
		String pHCNum = (String) session.getAttribute("hcNum");
		String pDOB = (String) session.getAttribute("pDOB");
		String DocName = "";
		
		try {
			Connection cnnct = DBConnection.connect();
			Statement stmtdn = cnnct.createStatement();
			
			String doc = "SELECT * FROM doctorprofile WHERE dFormName='" + DocFName + "'";
			ResultSet docInfo = stmtdn.executeQuery(doc);
			
			// Getting the doctor's full name from the database
			if(docInfo.next())
			{
				DocName = docInfo.getString("dName");
			}
		}
		
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		// Creating a new Appointment object with the information
		Appointment newAppt = new Appointment();
		newAppt.setaDate(aDate);
		newAppt.setaTime(aTime);
		newAppt.setdName(DocName);
		newAppt.setpName(PatName);
		newAppt.setpHCNum(pHCNum);
		newAppt.setdFName(DocFName);
		
		// Creating a new PatientList object with the information
		PatientList newPL = new PatientList();
		newPL.setdName(DocName);
		newPL.setpDOB(pDOB);
		newPL.setpHCNum(pHCNum);
		newPL.setpName(PatName);
		
		// Attempting to stored the appointment in the database
		try {
			app = appointmentDAO.StoreAppointment(newAppt);
		} 
		
		catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			Connection cnnct = DBConnection.connect();
			Statement stmtPD = cnnct.createStatement();
		
			String checkPD = "SELECT * FROM patientlist WHERE dName='" + DocName + "' and pName='" + PatName + "'";
			ResultSet PD = stmtPD.executeQuery(checkPD);
			
			if(PD.next()) {
				
			}
			
			else
			{
				// Adding the new PatientList information to the database if the patient is 
				// not yet a patient of this particular doctor
				try {
					PL = PatientListDAO.StorePatientList(newPL);
				}
				
				catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		// Removing the referral information for this patient once the appointment for the referral has been booked
		try {
			Connection cnnct = DBConnection.connect();
			String delete = "DELETE FROM referrals WHERE dName='" + DocFName + "' and pName ='" + PatName + "'";
			PreparedStatement pS = cnnct.prepareStatement(delete);
			
			stored = pS.executeUpdate();
		}
		
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		response.sendRedirect("http://localhost:8080/EasyHealth/PatientDashboard.jsp");
	}
}
