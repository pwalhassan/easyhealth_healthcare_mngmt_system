package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import database.DBConnection;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * The ViewFileServletPatient class which gets the patient name and file number from the select file web page, 
 * used by the Patient. The database row which corresponds with the selected file number will be used to set the 
 * information in the View File web page.
 */
public class ViewFileServletPatient extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Getting the patient's name and file number from the previous web page
		String pname = request.getParameter("pname");
		int filenum = Integer.parseInt(request.getParameter("filenum"));
		
		try {
			Connection cnnct = DBConnection.connect();
			
			Statement stmtpat = cnnct.createStatement();
			String showFiles = "SELECT * FROM patientfiles WHERE pName='" + pname + "'";
		
			// Executing the sql commands with the statement objects
			ResultSet patFiles = stmtpat.executeQuery(showFiles);
	
			String dName = "";
			String pName = "";
			String loc = "";
			String prac = "";
			String notes = "";
			String hcnum = "";
			String date = "";
			String rxs = "";
			String referrals = "";
			String follows = "";
			
			// Getting the file information from the database which corresponds to the selected file number (database row)
			while(patFiles.next()) {
				if(patFiles.getRow() == filenum) {
					System.out.print(filenum);
					dName = patFiles.getString("dName");
					pName = patFiles.getString("pName");
					loc = patFiles.getString("loc");
					prac = patFiles.getString("prac");
					notes = patFiles.getString("notes");
					hcnum = patFiles.getString("pHCN");
					date = patFiles.getString("apptDate");
					rxs = patFiles.getString("rx");
					referrals = patFiles.getString("referral");
					follows = patFiles.getString("ffUp");
					
					// Setting the information from the database to an HTTP session to be accessed by the view file web page
					HttpSession sessionf = request.getSession();
					sessionf.setAttribute("dName", dName);
					sessionf.setAttribute("pName", pName);
					sessionf.setAttribute("loc", loc);
					sessionf.setAttribute("prac", prac);
					sessionf.setAttribute("notes", notes);
					sessionf.setAttribute("hcnum", hcnum);
					sessionf.setAttribute("date", date);
					sessionf.setAttribute("rxs", rxs);
					sessionf.setAttribute("referrals", referrals);
					sessionf.setAttribute("follows", follows);
					
					response.sendRedirect("http://localhost:8080/EasyHealth/ViewFile.jsp");
				}
			}
		}
		
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
