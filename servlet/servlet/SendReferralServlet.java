package servlet;

import java.io.IOException;

import create.Referral;
import database.ReferralDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * The SendReferralServlet class which will get the doctor's name and patient's name from the send referral web page
 * created by a Doctor. The referral information will be stored in the database and will be accessed by the Patient
 * later, so that they can book an appointment with the referred doctor.
 */
public class SendReferralServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    private ReferralDAO ReferralDAO;
	
	public void init()
	{
		ReferralDAO = new ReferralDAO();
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// Getting the doctor's name and patient's name from the previous web page
		String docName = request.getParameter("dName");
		String patName = request.getParameter("pName");
		
		int ref = 0;
		
		// Creating a ne Referral object
		Referral newReferral = new Referral();
		newReferral.setdName(docName);
		newReferral.setpName(patName);
		
		// Attempting to store the referral information to the database
		try {
			ref = ReferralDAO.StoreReferral(newReferral);
			
			response.sendRedirect("http://localhost:8080/EasyHealth/DoctorDashboard.jsp");
		} 
		
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
