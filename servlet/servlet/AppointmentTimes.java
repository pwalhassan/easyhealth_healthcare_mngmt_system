package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import create.Appointment;
import create.PatientList;
import database.AppointmentDAO;
import database.DBConnection;
import database.PatientListDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/** 
 * The AppointmentTimesServlet which extracts information from the appointment time selection page
 * when a 'patient' user selects a time for their appointment with a doctor. The information collected
 * from this page using the servlet will be subsequently stored in the database.
 */
public class AppointmentTimes extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AppointmentDAO appointmentDAO;
	private PatientListDAO PatientListDAO;
	
	// Initializing new instances of the the Appointment and PatientList DAOs
	public void init()
	{
		appointmentDAO = new AppointmentDAO();
		PatientListDAO = new PatientListDAO();
	}
       
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int PL = 0;
		int app = 0;
		
		// An HTTP Session which will get the information entered in the previous web page.
		// This info will be used to create new Appointment and PatientList objects.
		HttpSession session = request.getSession();
		String PatName = (String) session.getAttribute("Name");
		String DocFName = (String) session.getAttribute("aDFName");
		String DocName = (String) session.getAttribute("aDName");
		String aTime = request.getParameter("appTime");
		String aDate = (String) session.getAttribute("aDate");
		String pHCNum = (String) session.getAttribute("hcNum");
		String pDOB = (String) session.getAttribute("pDOB");
		
		// Creating a new Appointment object
		Appointment newAppt = new Appointment();
		newAppt.setaDate(aDate);
		newAppt.setaTime(aTime);
		newAppt.setdName(DocName);
		newAppt.setpName(PatName);
		newAppt.setpHCNum(pHCNum);
		newAppt.setdFName(DocFName);
		
		// Creating a new PatientList object
		PatientList newPL = new PatientList();
		newPL.setdName(DocName);
		newPL.setpDOB(pDOB);
		newPL.setpHCNum(pHCNum);
		newPL.setpName(PatName);
		
		// Attempting to store the appointment info in the database
		try {
			app = appointmentDAO.StoreAppointment(newAppt);
		} 
		
		catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			Connection cnnct = DBConnection.connect();
			Statement stmtPD = cnnct.createStatement();
		
			String checkPD = "SELECT * FROM patientlist WHERE dName='" + DocName + "' and pName='" + PatName + "'";
			ResultSet PD = stmtPD.executeQuery(checkPD);
			
			if(PD.next()) {
				
			}
			
			// Attempting to store the PatientList information in the database if the patient 
			// does not exist in the patientlist table yet
			else {
				try {
					PL = PatientListDAO.StorePatientList(newPL);
				}
				
				catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		catch(Exception e) {		
			System.out.println(e.getMessage());
		}
		
		response.sendRedirect("http://localhost:8080/HealthCareManagementSystem/PatientDashboard.jsp");
	}

}
