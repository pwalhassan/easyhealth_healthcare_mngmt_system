package database;

import create.CreateDoctor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/** 
 * The Doctor data access object class which will help store the information collected 
 * at sign up into the database
 * 
 */
public class DoctorDAO {
	
	/** 
	 * The method used that will connect with the database and then store the 
	 * doctor information into the right table
	 * 
	 * @param newDoctor - the doctor object from which the sign up info will be taken
	 * 
	 * @return stored - the value to check that the info was stored successfully
	 * 
	 * @throws Exception - catching any exceptions that may have occurred while storing the info
	 * 
	 */
	public int StoreDoctor(CreateDoctor newDoctor) throws Exception{
		
		// Command to put the sign up information into the 'doctorprofile' table in the database
		// in the correct columns
		String insertDoctor = "INSERT INTO doctorprofile"
				+ "  (dUser, dEmail, dPass, dName, dSex, dSpec, dLicNum, dPrac, dLoc, dFormName) VALUES " 
				+ " (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

		// Value to check if the database table has been updated successfully
		int stored = 0;

		Class.forName("com.mysql.cj.jdbc.Driver");

		// Connecting to the database and entering/storing the sign up information into the table
		try {
			Connection cnnct = DBConnection.connect();
			
			// The prepared statement to check later to make sure that the info has been inserted successfully
			PreparedStatement pS = cnnct.prepareStatement(insertDoctor);
			
			// Adding the sign up info to the correctly numbered sections in the table
			pS.setString(1, newDoctor.getdUser());
			pS.setString(2, newDoctor.getdEmail());
			pS.setString(3, newDoctor.getdPass());
			if (newDoctor.getdMName() == " ") {
				pS.setString(4, newDoctor.getdFName() + " " + newDoctor.getdLName());
			}
			else {
				pS.setString(4, newDoctor.getdFName() + " " + newDoctor.getdMName() + " " 
				+ newDoctor.getdLName());
			}
			pS.setString(5, newDoctor.getdSex());
			pS.setString(6, newDoctor.getdSpecialty());
			pS.setString(7, newDoctor.getdLicense());
			pS.setString(8, newDoctor.getdPractice());
			pS.setString(9, newDoctor.getdLocation());
			pS.setString(10, "Dr."+ " " + newDoctor.getdLName());

			// Updating the storage value using the prepared statement's update method
			stored = pS.executeUpdate();
		} 
		
		// Catching any exceptions thrown while connecting to the database
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return stored;
	}
}