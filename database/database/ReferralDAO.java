package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import create.Referral;

/** 
 * The Referral data access object file which will help store the information collected 
 * when an referral is created into the database
 * 
 */
public class ReferralDAO {
	
	/** 
	 * The method used that will connect with the database and then store the 
	 * referral information into the right table
	 * 
	 * @param newReferral - the referral object from which the referral info will be taken
	 * 
	 * @return stored - the value to check that the info was stored successfully
	 * 
	 * @throws Exception - catching any exceptions that may have occurred while storing the info
	 * 
	 */
	public int StoreReferral(Referral newReferral) throws Exception{
		
		// Command to put the referral information into the 'referrals' table in the database
		// in the correct columns
		String insertReferral = "INSERT INTO referrals"
				+ "  (dName, pName) VALUES " 
				+ " (?, ?);";

		// Value to check if the database table has been updated successfully
		int stored = 0;

		Class.forName("com.mysql.cj.jdbc.Driver");

		// Connecting to the database and entering/storing the referral information into the table
		try {
			Connection cnnct = DBConnection.connect();
			
			// The prepared statement to check later to make sure that the info has been inserted successfully
			PreparedStatement pS = cnnct.prepareStatement(insertReferral);
			
			// Adding the sign up info to the correctly numbered sections in the table
			pS.setString(1, newReferral.getdName());
			pS.setString(2, newReferral.getpName());

			// Updating the storage value using the prepared statement's update method
			stored = pS.executeUpdate();
		} 
		
		// Catching any exceptions thrown while connecting to the database
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return stored;
	}
}
