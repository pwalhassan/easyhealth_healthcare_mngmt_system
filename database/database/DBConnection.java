package database;

import java.sql.*;
import java.sql.DriverManager;

/** 
 * The Database Connection class to establish a connection with the database.
 * This will allow proper storage of information into the database as well as 
 * provide a means to query the database and retrieve information stored.
 * 
 */
public class DBConnection {
	private static final String dBDriver = "com.mysql.cj.jdbc.Driver";
	private static final String dBURL = "jdbc:mysql://localhost:3306/signupinfo";
	private static final String userName = "root";
	private static final String passWord = "29inWonderlandWoohoo21";

	private static Connection connection;

	/** 
	 * Connecting to the database 
     * 
     * @return connection if successful
     * 
     * @throws Exception - to catch any exceptions thrown while trying to create the connection
     * 
     */
    public static Connection connect() throws Exception{
		try {
	      Class.forName(dBDriver);
	      
	      // Establishing the connection using the database properties
	      connection = DriverManager.getConnection(dBURL, userName, passWord);
		}
		
		// Catching any exceptions thrown while establishing the connection
		catch (Exception e)
		{
			e.printStackTrace();
		}
	    return connection;
    }

	/** 
	 * Disconnecting the database
	 * 
	 * @throws SQLException catching any errors thrown while closing the connection
	 * 
	 */
	public void disconnect() {
		try {
			connection.close();
		} 
			
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

