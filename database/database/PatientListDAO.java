package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import create.PatientList;

/** 
 * The Patient List data access object file which will help store the information 
 * of a doctors patients into the database
 * 
 */
public class PatientListDAO {
	
	/** 
	 * The method used that will connect with the database and then store the 
	 * Patient/Doctor information into the right table
	 * 
	 * @param newPatientList - the PatientList object from which the Patient/Doctor info will be taken
	 * 
	 * @return stored - the value to check that the info was stored successfully
	 * 
	 * @throws Exception - catching any exceptions that may have occurred while storing the info
	 * 
	 */
	public int StorePatientList(PatientList newPatientList) throws Exception{
		
		// Command to put the patient list information into the 'patientlist' table in the database
		// in the correct columns
		String insertAppointment = "INSERT INTO patientlist"
				+ "  (dName, pName, pHCNum, pDOB) VALUES " 
				+ " (?, ?, ?, ?);";

		// Value to check if the database table has been updated successfully
		int stored = 0;

		Class.forName("com.mysql.cj.jdbc.Driver");

		// Connecting to the database and entering/storing the patient list information into the table
		try {
			Connection cnnct = DBConnection.connect();
			
			// The prepared statement to check later to make sure that the info has been inserted successfully
			PreparedStatement pS = cnnct.prepareStatement(insertAppointment);
			
			// Adding the sign up info to the correctly numbered sections in the table
			pS.setString(1, newPatientList.getdName());
			pS.setString(2, newPatientList.getpName());
			pS.setString(3, newPatientList.getpHCNum());
			pS.setString(4, newPatientList.getpDOB());
			

			// Updating the storage value using the prepared statement's update method
			stored = pS.executeUpdate();
		} 
		
		// Catching any exceptions thrown while connecting to the database
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return stored;
	}
}
