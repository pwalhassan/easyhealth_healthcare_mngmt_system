package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import create.CreateFile;

/**
 * The Patient File data access object class which will be used to store the information entered
 * into the Patient File form by a Doctor, into the database.
 *
 */
public class PatientFileDAO {
	
	/** 
	 * The method used that will connect with the database and then store the 
	 * file information into the right table
	 * 
	 * @param newFile - the patient file object from which the file info will be taken
	 * 
	 * @return stored - the value to check that the info was stored successfully
	 * 
	 * @throws Exception - catching any exceptions that may have occurred while storing the info
	 * 
	 */
	public int StoreFile(CreateFile newFile) throws Exception{
			
		// Command to put the patient file information into the 'patientfiles' table in the database
		// in the correct columns
		String addFile = "INSERT INTO patientfiles"
				+ "  (dName, pName, pHCN, apptDate, notes, rx, referral, ffUp, loc, prac) VALUES " 
				+ " (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

		// Value to check if the database table has been updated successfully
		int stored = 0;

		Class.forName("com.mysql.cj.jdbc.Driver");

		// Connecting to the database and entering/storing the file information into the table
		try {
			Connection cnnct = DBConnection.connect();
			
			// The prepared statement to check later to make sure that the info has been inserted successfully
			PreparedStatement pS = cnnct.prepareStatement(addFile);
			
			// Adding the file info to the correctly numbered sections in the table
			pS.setString(1, newFile.getDrName());
			pS.setString(2, newFile.getPatName());
			pS.setString(3, newFile.getPatHCN());
			pS.setString(4, newFile.getDate());
			pS.setString(5, newFile.getNotes());
			pS.setString(6, newFile.getRxNew());
			pS.setString(7, newFile.getAnyReferral());
			pS.setString(8, newFile.getFollowUp());
			pS.setString(9, newFile.getLoc());
			pS.setString(10, newFile.getPrac());

			// Updating the storage value using the prepared statement's update method
			stored = pS.executeUpdate();
		} 
		
		// Catching any exceptions thrown while connecting to the database
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return stored;
	}
}
