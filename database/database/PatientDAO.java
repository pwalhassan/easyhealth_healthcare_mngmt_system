package database;

import create.CreatePatient;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/** 
 * The Patient data access object class which will help store the information collected 
 * at sign up into the database
 * 
 */
public class PatientDAO {

	/** 
	 * The method used that will connect with the database and then store the 
	 * patient information into the right table
	 * 
	 * @param newPatient - the patient object from which the sign up info will be taken
	 * 
	 * @return stored - the value to check that the info was stored successfully
	 * 
	 * @throws Exception - catching any exceptions that may have occurred while storing the info
	 * 
	 */
	public int StorePatient(CreatePatient newPatient) throws Exception{
		
		// Command to put the sign up information into the 'patientprofile' table in the database
		// in the correct columns
		String insertPatient = "INSERT INTO patientprofile"
				+ "  (pUser, pEmail, pPass, pName, pSex, pDOB, pAdd1, pHCNum, pOrgD) VALUES " 
				+ " (?, ?, ?, ?, ?, ?, ?, ?, ?);";

		// Value to check if the data has been stored successfully
		int stored = 0;

		Class.forName("com.mysql.cj.jdbc.Driver");

		// Connecting with the database and entering/storing the sign up info into the table
		try {
			Connection cnnct = DBConnection.connect();
			
			// The prepared statement to check later to make sure that the info has been inserted successfully
			PreparedStatement pS = cnnct.prepareStatement(insertPatient);
			
			// Adding the sign up info to the correctly numbered sections in the table
			pS.setString(1, newPatient.getpUser());
			pS.setString(2, newPatient.getpEmail());
			pS.setString(3, newPatient.getpPass());
			if (newPatient.getpMName() == "") {
				pS.setString(4, newPatient.getpFName() + " " + newPatient.getpLName());
			}
			else {
				pS.setString(4, newPatient.getpFName() + " " + newPatient.getpMName() + " " 
				+ newPatient.getpLName());
			}
			pS.setString(5, newPatient.getpSex());
			pS.setString(6, newPatient.getpDOB());
			if (newPatient.getpAddl2() == "") {
				pS.setString(7, newPatient.getpAddl1() + ", " + newPatient.getpCity() + ", " + 
				newPatient.getpProv() + ", " + newPatient.getpCntry() + ", " + newPatient.getpPcode());
			}
			else {
				pS.setString(7, newPatient.getpAddl1() + ", " + newPatient.getpAddl2() + ", "
				+ newPatient.getpCity() + ", " + newPatient.getpProv() + ", " + newPatient.getpCntry()
				+ ", " + newPatient.getpPcode());
			}
			pS.setString(8, newPatient.getHCNum());
			pS.setString(9, newPatient.getOrgD());

			// Updating the storage value using the prepared statement's update method
			stored = pS.executeUpdate();
		} 
		
		// Catching any exceptions thrown while trying to store the information
		catch (SQLException e) {
			e.printStackTrace();;
		}
		
		return stored;
	}
}
