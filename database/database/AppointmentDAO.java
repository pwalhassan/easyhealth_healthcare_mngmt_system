package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import create.Appointment;

/** 
 * The Appointment data access object class which will help store the information collected 
 * when an appointment is created into the database
 * 
 */
public class AppointmentDAO {
	
	/** 
	 * The method used that will connect with the database and then store the 
	 * appointment information into the right table
	 * 
	 * @param newAppointment - the appointment object from which the appointment info will be taken
	 * 
	 * @return stored - the value to check that the info was stored successfully
	 * 
	 * @throws Exception catching any exceptions that may have occurred while storing the info
	 * 
	 */
	public int StoreAppointment(Appointment newAppointment) throws Exception{
		
		// Command to put the appoinment information into the 'appointments' table in the database
		// in the correct columns
		String insertAppointment = "INSERT INTO appointments"
				+ "  (dName, dFName, pName, pHCNum, aTime, aDate) VALUES " 
				+ " (?, ?, ?, ?, ?, ?);";

		// Value to check if the database table has been updated successfully
		int stored = 0;

		Class.forName("com.mysql.cj.jdbc.Driver");

		// Connecting to the database and entering/storing the appointment information into the table
		try {
			Connection cnnct = DBConnection.connect();
			
			// The prepared statement to check later to make sure that the info has been inserted successfully
			PreparedStatement pS = cnnct.prepareStatement(insertAppointment);
			
			// Adding the appointment info to the correctly numbered sections in the table
			pS.setString(1, newAppointment.getdName());
			pS.setString(2, newAppointment.getdFName());
			pS.setString(3, newAppointment.getpName());
			pS.setString(4, newAppointment.getpHCNum());
			pS.setString(5, newAppointment.getaTime());
			pS.setString(6, newAppointment.getaDate());
			
			// Updating the storage value using the prepared statement's update method
			stored = pS.executeUpdate();
		} 
		
		// Catching any exceptions thrown while connecting to the database
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return stored;
	}
}
